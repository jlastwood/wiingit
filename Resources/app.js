var Globals = require("/lib/Globals");

var InviteScreen = require("/ui/iphone/InviteScreen");
var AcceptedScreen = require("/ui/iphone/AcceptedScreen");
var InviteScreenAndroid = require("/ui/android/InviteScreen");
var AcceptedScreenAndroid = require("/ui/android/AcceptedScreen");

var Cloud = require('ti.cloud');

//Cloud.debug = true;

var MainController;

if (Ti.Platform.osname === 'android') {

    Ti.API.info("STATUS: Hello");

    MainController = require('ui/android/MainController');
    MainController();

    var CloudPush = require('ti.cloudpush');

    CloudPush.retrieveDeviceToken({
        success: function deviceTokenSuccess(e) {

            Ti.App.Properties.setString("pushToken", e.deviceToken);

        },
        error: function deviceTokenError(e) {
            alert('Failed to register for push! ' + e.error);
        }

    });

    // Event monitoring Android push notifications
    // c - command, id - userid, n - name

    CloudPush.addEventListener('callback', function(e) {

        var answer = JSON.parse(e.payload);

        if (answer.c == "invite") {

            Ti.API.info("STATUS: Invite from push");

            new InviteScreenAndroid(answer.id, answer.n, answer.f).open();

        }

        if (answer.c == "confirm") {

            Ti.API.info("STATUS: Confirm from push");

            Ti.App.Properties.setBool('accepted', true);

            new AcceptedScreenAndroid(answer.id, answer.n, answer.f).open();

        }

        if (answer.c == "decline") {

            Ti.API.info("STATUS: Decline from push");

            Ti.App.Properties.removeProperty('currentWing');
            Ti.App.Properties.removeProperty('accepted');
            alert("Sorry! " + answer.n + " declined to become your Wing...");
        }

        if (answer.c == "unwing") {

            Ti.API.info("STATUS: Remove from push");

            Ti.App.Properties.removeProperty('currentWing');
            Ti.App.Properties.removeProperty('accepted');

            alert("got uniwing from " + answer.id);

        }

        if (answer.c == "chat") {

            Ti.API.info("STATUS: Chat from push");

            var ids = answer.ids.split(",");

            ids.splice(ids.indexOf(Ti.App.Properties.getString('currentUserId')), 1);
            ids.splice(ids.indexOf(Ti.App.Properties.getString('currentWing')), 1);

            if (!Ti.App.Properties.getBool("chatActive")) {

                var MessageScreenAndroid = require("/ui/android/MessageScreen");

                new MessageScreenAndroid(ids[0], ids[1]).open();

            }

        }

    });

} else { // iPhone

    Titanium.UI.iPhone.statusBarStyle = Titanium.UI.iPhone.StatusBar.OPAQUE_BLACK;

    function generalStartupCheck() {

        Ti.API.info("STATUS: Startup check");

        if (!Titanium.Network.online) {

            var alertDialog = Titanium.UI.createAlertDialog({
                title: 'WARNING!',
                message: 'Your device is not online.',
                buttonNames: ['Go online and click OK']
            });

            alertDialog.addEventListener('click', generalStartupCheck);

            alertDialog.show();

        } else {

            MainController = require('ui/iphone/MainController');

            MainController();

        }

    }


    Titanium.Network.registerForPushNotifications({

        types: [Titanium.Network.NOTIFICATION_TYPE_BADGE, Titanium.Network.NOTIFICATION_TYPE_ALERT, Titanium.Network.NOTIFICATION_TYPE_SOUND],

        success: function(e) {

            var deviceToken = e.deviceToken;
            Ti.App.Properties.setString("pushToken", deviceToken);

        },
        error: function(e) {
            alert("Error during registration: " + e.error);
        },
        callback: function(e) {

            // Event monitoring iPhone push notifications
            // c - command, id - userid, n - name

            Ti.API.info("STATUS: iOS Push");

            if (e.data.c == "invite") {

                new InviteScreen(e.data.id, e.data.n, e.data.f).open();

            }

            if (e.data.c == "confirm") {

                Ti.App.Properties.setBool('accepted', true);

                new AcceptedScreen(e.data.id, e.data.n, e.data.f).open();

            }

            if (e.data.c == "decline") {

                Ti.App.Properties.removeProperty('currentWing');
                Ti.App.Properties.removeProperty('accepted');
                alert("Sorry! " + e.data.n + " declined to become your Wing...");
            }

            if (e.data.c == "unwing") {

                Ti.App.Properties.removeProperty('currentWing');
                Ti.App.Properties.removeProperty('accepted');

                alert("got uniwing from " + e.data.id);

            }

            if (e.data.c == "chat") {

                var ids = e.data.ids.split(",");

                ids.splice(ids.indexOf(Ti.App.Properties.getString('currentUserId')), 1);
                ids.splice(ids.indexOf(Ti.App.Properties.getString('currentWing')), 1);

                if (!Ti.App.Properties.getBool("chatActive")) {

                    var MessageScreen = require("/ui/iphone/MessageScreen");

                    new MessageScreen(ids[0], ids[1]).open();

                }

            }

        }

    });

    generalStartupCheck();

}