var Globals = require("/lib/Globals");

var Header = require("/ui/android/Header");

function UnwingScreen(fbid, friendName) {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
	});

	var upperHeader = new Header();

	self.add(upperHeader);

	var photos = Ti.UI.createView({
		width: Ti.UI.FILL,
		top: 80
	});

	var userImage = Ti.UI.createImageView({
		image: "http://graph.facebook.com/" + fbid + "/picture?width=256&height=256",
		top: 30,
		left: 20,
		width: 128,
		height: 128,
		preventDefaultImage: true
	});

	var myImage = Ti.UI.createImageView({
		image: "http://graph.facebook.com/" + Ti.App.Properties.getString('fbid') + "/picture?width=256&height=256",
		top: 30,
		right: 20,
		width: 128,
		height: 128,
		preventDefaultImage: true
	});

	photos.add(userImage);
	photos.add(myImage);

	self.add(photos);


	var acceptText = Ti.UI.createLabel({
		text: "Are you sure you want to Un-Wing " + friendName + "?",
		color: "#4dc8f4",
		font: {
			fontSize: 28,
			fontWeight: 'bold'
		},
		top: 260,
		left: 10,
		right: 10,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
	});

	self.add(acceptText);
	

	var subText = Ti.UI.createLabel({
		text: "You will loose all active matches",
		color: "#4dc8f4",
		font: {
			fontSize: 16,
			fontWeight: 'normal'
		},
		top: 360,
		left: 10,
		right: 10,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
	});

	self.add(subText);



	var declineButton = Ti.UI.createButton({
		bottom: 10,
		left:20,
		title: "Decline",
		color: "#4dc8f4",
		font: {
			fontSize: 18,
			fontWeight: 'bold'
		},
		style: Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});

	self.add(declineButton);


	var acceptButton = Ti.UI.createButton({
		bottom: 10,
		right:20,
		title: "Accept",
		color: "#4dc8f4",
		font: {
			fontSize: 18,
			fontWeight: 'bold'
		},
		style: Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});

	self.add(acceptButton);


	acceptButton.addEventListener('click', function() {

		Ti.App.Properties.removeProperty("wingid");
		Ti.App.Properties.removeProperty("wingname");
		Ti.App.Properties.removeProperty("wingphoto");		
		Ti.App.Properties.removeProperty("wingfbid");	

		Ti.App.fireEvent("gotostart");		

		self.close();

	});


	declineButton.addEventListener('click', function() {

		self.close();

	});	
	
	

	Ti.App.addEventListener('logMeOut', function(){
		
		self.close();
		
	});

	return self;
}

module.exports = UnwingScreen;
