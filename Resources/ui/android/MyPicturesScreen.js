var Globals = require("/lib/Globals");

var Header = require("/ui/android/Header");

var PhotoFrame = require("/ui/android/PhotoFrame");

function MyPicturesScreen(friendName) {


	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
	});

	var upperHeader = new Header("My Pictures", 1, self);

	self.add(upperHeader);
	
	var profileImage = new PhotoFrame({
		top: 100,
		theSize : 240
	});
	
	self.add(profileImage);

	/*var profileImage = Ti.UI.createImageView({
		top:10,
		left:10,
		right:10,
		bottom:10,
		preventDefaultImage: true
		//image: "http://graph.facebook.com/" + Ti.App.Properties.getString('fbid') +
		// "/picture?width=480&height=480",
	});

	var photosView = Ti.UI.createView({
		backgroundColor: 'white',
		width: 240,
		height: 240,
		top: 100
	});

	photosView.add(profileImage);

	var picAction = Ti.UI.createButton({
		width: 36 * .66,
		height: 36 * .66,
		backgroundImage: "/img/picDel.png",
		top: 0,
		right: 0
	});

	photosView.add(picAction);
	
	self.add(photosView);
	
	*/
	


	var smallList = [];

	var small1 = new PhotoFrame({
		top: 350,
		left: 10,
		theSize: 100
	});

	self.add(small1);

	smallList.push(small1);

	var small2 = new PhotoFrame({
		top: 350,
		theSize : 100
	});

	self.add(small2);

	smallList.push(small2);

	var small3 = new PhotoFrame({
		top: 350,
		right: 10,
		theSize : 100
	});

	self.add(small3);
	smallList.push(small3);
	
	// get pictures

	Cloud.Photos.query({
		page: 1,
		per_page: 20,
		where: {
			//tags_array: "profile",
			user_id: Ti.App.Properties.getString('currentUserId')
		}
	}, function(e) {
		if (e.success) {

			//	alert(e.photos.length);

			var temp = [];

			for (var i = 0; i < e.photos.length; i++) {
				var photo = e.photos[i];

				//alert(photo.urls.medium_640);

				if (photo.tags && photo.tags.indexOf("profile") !== -1) {
					
					profileImage.addPhoto(photo.urls.medium_640, photo.id);

					//profileImage.setImage(photo.urls.medium_640);
					//profileImage.photoCloudId = photo.id;

				} else {

					temp.push({
						photo: photo.urls.medium_640,
						id: photo.id
					});
				}

			}

			for (var i = 0; i < 3; i++) {

				if (temp[i] && smallList[i]) {

					smallList[i].addPhoto(temp[i].photo, temp[i].id);

				}

			}

		} else {
			alert('UPL Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});

	Ti.App.addEventListener('logMeOut', function() {

		self.close();

	});

	return self;
}

module.exports = MyPicturesScreen;
