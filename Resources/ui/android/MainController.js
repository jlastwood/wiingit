var Globals = require("/lib/Globals");

var LoginWithFB = require("/connections/LoginWithFBAndroid");

function MainController() {

	var Window;
	
	// first start	

	if (!Ti.App.Properties.hasProperty("wasRun")) {
		
		Ti.API.info('STATUS: first start');

		Titanium.App.Properties.setBool("wasRun", true);

		Window = require("/ui/android/DemoBrowserScreen");
		new Window().open();

	// app has been started, but not logged in now

	} else if (!Ti.App.Properties.hasProperty("fbid")) {
		
		Ti.API.info('STATUS: Not logged in');

		Window = require('/ui/android/LandingScreen');
		new Window().open();

	// is logged in, has wing	

	} else if (Ti.App.Properties.getBool('accepted')==true) {
		
		Ti.API.info('STATUS: Logged in and has Wing');
		
		new LoginWithFB(function() {
			
			Ti.API.info("STATUS: Successfully logged in. Starting profile browser.");
			
			Window = require("/ui/android/ProfileBrowserScreen");
			new Window().open();
			
		});	
		
	// is logged in, no wing			
		
	} else {	
		
		Ti.API.info("STATUS: Logged in but has no Wing");

		new LoginWithFB(function() {
			
			Ti.API.info("STATUS: Successfully logged in. Starting wing selector.");
			
			Window = require('/ui/android/YourWingScreen');
			new Window().open();

		});

	}

}

module.exports = MainController;
