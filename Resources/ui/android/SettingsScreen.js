var Globals = require("/lib/Globals");

var Header = require("/ui/android/Header");

var PreferencesScreen = require("/ui/android/PreferencesScreen");
var MyProfileScreen = require("/ui/android/MyProfileScreen");

var UnwingMe = require("/connections/UnwingMe");

function SettingsScreen() {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
		layout: 'vertical'
	});

	self.addEventListener('open', function() {

	});

	var upperHeader = new Header("Settings", 1, self);

	self.add(upperHeader);

	var prefsButton = Ti.UI.createButton({
		title: "Preferences",
		color: "#bcbec0",
		font: {
			fontSize: 24,
			fontWeight: 'normal'
		},
		top: 20,
		backgroundImage: "/img/empty.png",
		left: 20
	});

	self.add(prefsButton);

	var profileButton = Ti.UI.createButton({
		title: "My Profile",
		color: "#bcbec0",
		font: {
			fontSize: 24,
			fontWeight: 'normal'
		},
		top: 10,
		left: 20,
		backgroundImage: "/img/empty.png",
	});

	self.add(profileButton);

	var inviteButton = Ti.UI.createView({
		top: 10,
		left: 20,
		layout: 'horizontal',
		height: Ti.UI.SIZE
	});

	var inviteText = Ti.UI.createLabel({
		text: "Invite Friends to",
		color: "#bcbec0",
		font: {
			fontSize: 24,
			fontWeight: 'normal'
		},
		top: 0,
		left: 0
	});

	inviteButton.add(inviteText);

	var inviteLogo = Ti.UI.createImageView({
		image: '/img/smallLogo.png',
		height: 24,
		width: 86,
		top: 6
	});

	inviteButton.add(inviteLogo);

	self.add(inviteButton);

	var rateButton = Ti.UI.createView({
		top: 20,
		left: 20,
		layout: 'horizontal',
		height: Ti.UI.SIZE
	});

	var rateText = Ti.UI.createLabel({
		text: "Rate",
		color: "#bcbec0",
		font: {
			fontSize: 24,
			fontWeight: 'normal'
		},
		top: 0,
		left: 0
	});

	rateButton.add(rateText);

	var rateLogo = Ti.UI.createImageView({
		image: '/img/smallLogo.png',
		height: 24,
		width: 86,
		top: 6
	});

	rateButton.add(rateLogo);

	self.add(rateButton);

	if (Ti.App.Properties.hasProperty('currentWing')) {

		var unwingButton = Ti.UI.createButton({
			title: "Unwing",
			color: "#bcbec0",
			font: {
				fontSize: 24,
				fontWeight: 'normal'
			},
			top: 20,
			left: 20,
			backgroundImage: '/img/empty.png'
		});

		self.add(unwingButton);

	}

	var deleteButton = Ti.UI.createButton({
		title: "Delete your account",
		color: "#bcbec0",
		font: {
			fontSize: 24,
			fontWeight: 'normal'
		},
		top: 20,
		left: 20,
		backgroundImage: "/img/empty.png",
	});

	self.add(deleteButton);

	// controllers

	prefsButton.addEventListener('click', function() {

		new PreferencesScreen().open();

	});

	profileButton.addEventListener('click', function() {

		new MyProfileScreen().open();

	});

	inviteButton.addEventListener('click', function() {

		var emailDialog = Ti.UI.createEmailDialog();
		emailDialog.subject = "WingIt Rocks!";
		emailDialog.toRecipients = [];
		emailDialog.messageBody = 'Download it!';
		emailDialog.open();

	});

	rateButton.addEventListener('click', function() {

		alert("Awaiting AppStore URL");

	});

	deleteButton.addEventListener('click', function() {

		Ti.App.Properties.removeProperty("fbid");
		Ti.App.Properties.removeProperty("gender");
		Ti.App.Properties.removeProperty("birthday");
		Ti.App.Properties.removeProperty("name");
		Ti.App.Properties.removeProperty("facebookFriends");
		Ti.App.Properties.removeProperty("accepted");

		Ti.App.Properties.removeProperty("preferredGender");
		Ti.App.Properties.removeProperty("minAge");
		Ti.App.Properties.removeProperty("maxAge");
		Ti.App.Properties.removeProperty("maxDistance");

		new UnwingMe({
			user: Ti.App.Properties.getString("currentUserId")
		}, function() {

		});

		Cloud.Friends.remove({
			user_ids: Ti.App.Properties.removeProperty("currentWing")
		}, function(e) {
			if (e.success) {
				
			} else {
				//alert('Error:\\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

		Ti.App.Properties.removeProperty("currentWing");

		Ti.App.fireEvent("logMeOut");

		var LandingScreen = require('/ui/android/LandingScreen');

		self.close();

		new LandingScreen().open();

	});

	if (Ti.App.Properties.hasProperty('currentWing')) {

		unwingButton.addEventListener('click', function() {

			// remove my wing on cloud

			Cloud.Friends.remove({
				user_ids: Ti.App.Properties.getString('currentWing'),
				approval_required: false
			}, function(e) {
				if (e.success) {

					new UnwingMe({
						user: Ti.App.Properties.getString("currentUserId")
					}, function() {

					});

					Ti.App.Properties.removeProperty("currentWing");

					Ti.App.fireEvent("logMeOut");

					var YourWingScreen = require('/ui/android/YourWingScreen');

					self.close();

					new YourWingScreen().open();

				} else {
					alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
				}
			});

		});

	}

	return self;
}

module.exports = SettingsScreen;
