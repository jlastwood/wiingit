var Globals = require("/lib/Globals");

var Header = require("/ui/android/Header");

var IndividualScreen = require("/ui/android/IndividualScreen");

function YourMatchesScreen() {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
		layout: 'vertical'
	});

	var upperHeader = new Header();

	self.add(upperHeader);
	
	
	function viewMaker(){
		
		
		var view = Ti.UI.createView({
			width:Ti.UI.FILL,
			height:Ti.UI.FILL,
			layout: 'vertical'
		});
		
		var img1 = Ti.UI.createImageView({
			width: 600/2,
			height:392/2,
			top:20,
			image:'/img/example1.png'
		});


		var img2 = Ti.UI.createImageView({
			width: 600/2,
			height:392/2,
			top:20,
			image:'/img/example2.png'
		});
		
		view.add(img1);
		view.add(img2);
		
		view.addEventListener('click',function(){
			
			new IndividualScreen().open();
			
		});
		
		return view;
		
	}
	
	

	////////

	var isAndroid = Ti.Platform.osname === 'android';

	var currentDate = new Date(), msIntervalBetweenViews = 1000 * 60 * 60 * 24;

	var scrollable = Ti.UI.createScrollableView({
		top: 20,
		currentPage: 1,
		showPagingControls: true,
		pagingControlHeight: 30
	}), containers = [Ti.UI.createView({
		backgroundColor: '#fff',
		top: 0,
		right: 0,
		bottom: 0,
		left: 0
	}), Ti.UI.createView({
		backgroundColor: '#fff',
		top: 0,
		right: 0,
		bottom: 0,
		left: 0
	}), Ti.UI.createView({
		backgroundColor: '#fff',
		top: 0,
		right: 0,
		bottom: 0,
		left: 0
	})];

	self.add(scrollable);
	scrollable.views = containers;

	/**
	 * Loads data into the specified view based on the specified date.
	 * @param view
	 * @param date
	 */
	function loadView(view, date) {
		// empty out any children
		if (view.children) {
			for (var c = view.children.length - 1; c >= 0; c--) {
				view.remove(view.children[c]);
			}
		}
		// add new children
		var v = new viewMaker();
		view.add(v);
	}

	/**
	 * Whenever we scroll, manipulate our views so that the user is back to viewing
	 * the "middle" view with a buffer view on
	 * either side, then make sure the buffer views are actually loaded and ready to
	 * go.
	 */
	function scrollListener(evt) {
		// what is our current page?
		switch (evt.currentPageAsFloat) {
			case 0:
				// scrolled to the left
				// so pop a view off the end, and put it at the start
				containers.unshift(containers.pop());
				if (isAndroid) {
					// temporarily remove our event listener (for Android's sake...)
					scrollable.removeEventListener('scroll', scrollListener);
				}
				// reset the counter so we are back in the middle
				scrollable.currentPage = 1;
				// reset our views array
				scrollable.views = containers;
				if (isAndroid) {
					// now we can add the event listener again
					scrollable.addEventListener('scroll', scrollListener);
				}
				// take a day from our currentDate
				currentDate.setDate(currentDate.getDate() - 1);
				// and now buffer load the view we reset
				loadView(containers[0], new Date(currentDate.getTime() - msIntervalBetweenViews));
				break;
			case 1:
				// they didn't go anywhere; should only happen the first time around
				break;
			case 2:
				// scrolled to the right
				// so shift a view off the start, and put it at the end
				containers.push(containers.shift());
				if (isAndroid) {
					// temporarily remove our event listener (for Android's sake...)
					scrollable.removeEventListener('scroll', scrollListener);
				}
				// reset the counter so we are back in the middle
				scrollable.currentPage = 1;
				// reset our views array
				scrollable.views = containers;
				if (isAndroid) {
					// now we can add the event listener again
					scrollable.addEventListener('scroll', scrollListener);
				}
				// add a day to our currentDate
				currentDate.setDate(currentDate.getDate() + 1);
				// and now buffer load the view we reset
				loadView(containers[2], new Date(currentDate.getTime() + msIntervalBetweenViews));
				break;
		}
	}


	scrollable.addEventListener('scroll', scrollListener);

	/**
	 * Set up our initial views.
	 */
	loadView(containers[0], new Date(currentDate.getTime() - msIntervalBetweenViews));
	loadView(containers[1], currentDate);
	loadView(containers[2], new Date(currentDate.getTime() + msIntervalBetweenViews));

	////////

	Ti.App.addEventListener('logMeOut', function(){
		
		self.close();
		
	});

	return self;
}

module.exports = YourMatchesScreen;
