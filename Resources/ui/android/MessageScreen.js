var Globals = require("/lib/Globals");

var Header = require("/ui/android/Header");

var cE = require("/ui/android/ChatElements");

function MessageScreen(match1, match2) {

	var ChatElements = new cE();

	ChatElements.setUpdateTime(0);

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
	});

	self.addEventListener("focus", function() {

		Ti.App.Properties.setBool("chatActive",true);

	});

	self.addEventListener("blur", function() {

		Ti.App.Properties.setBool("chatActive",false);

	});

	var upperHeader = new Header("Messages", 1, self);

	self.add(upperHeader);

	var outerScroll = Ti.UI.createScrollView({
		top: 100,
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		layout: 'vertical',
		canCancelEvents: false
	});

	self.add(outerScroll);

	ChatElements.getAllPhotos(Ti.App.Properties.getString('currentUserId'), Ti.App.Properties.getString('currentWing'), match1, match2, function(thePhotos) {

		var ids = [];

		ids.push(Ti.App.Properties.getString('currentUserId'));
		ids.push(Ti.App.Properties.getString('currentWing'));
		ids.push(match1);
		ids.push(match2);

		//alert(ids);

		ChatElements.getMessagesSince(ids, ChatElements.getUpdateTime(), function(messages) {

			ChatElements.putMessages(messages, scrollPad, thePhotos, ids);

		});

		function refreshChat() {

			ChatElements.getMessagesSince(ids, ChatElements.getUpdateTime(), function(messages) {

				ChatElements.putMessages(messages, scrollPad, thePhotos, ids);

			});

		}

		var repeater = setInterval(refreshChat, 2000);

		self.addEventListener("close", function() {

			clearInterval(repeater);

		});

		var scrollPad = Ti.UI.createScrollView({
			top: 0,
			width: Ti.UI.FILL,
			height: Ti.Platform.displayCaps.platformHeight == 568 ? 568 - 150 : 480 - 150,
			layout: 'vertical',
		});

		outerScroll.add(scrollPad);

		var entryPad = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: 50,
			top: 0,
		});

		var entryField = Ti.UI.createTextField({
			width: 546 / 2,
			height: 64 / 2,
			paddingLeft: 20,
			paddingRight: 20,
			left: 5,
			backgroundImage: "/img/whiteBubble.png"
		});

		entryPad.add(entryField);

		var entryButton = Ti.UI.createButton({
			color: "#4dc8f4",
			font: {
				fontSize: 13,
				fontWeight: 'bold'
			},
			title: "Send",
			style: Titanium.UI.iPhone.SystemButtonStyle.PLAIN,
			right: 2,
			backgroundImage:'/img/empty.png'
		});

		entryPad.add(entryButton);

		outerScroll.add(entryPad);

		function sendMessage(message, toUsers, callback) {

			//alert("sending " + ids.join(','));

			Cloud.Chats.create({
				to_ids: ids.join(','),
				message: message,
				channel: "wingit",
				payload: {
					"alert": Ti.App.Properties.getString("name").split(" ")[0] + ": " + message.substring(0, 25) + "...",
					"c": "chat",
					"ids": ids.join(',')
				}
			}, function(e) {

				if (e.success) {

					callback(1);

					//ChatElements.getMessagesSince(ids, ChatElements.getUpdateTime(),
					// function(messages) {

					//	ChatElements.putMessages(messages, scrollPad, thePhotos);

					//});

					//	scrollPad.add(new
					// ChatElements.blueBubble(Ti.App.Properties.getString('currentUserId'), message,
					// thePhotos.me));

					//	setTimeout(function() {

					//		scrollPad.scrollToBottom();

					//	}, 300);

					//alert('Success:\n' + 'From: ' + chat.from.first_name + ' ' +
					// chat.from.last_name + '\n' + 'Updated: ' + chat.updated_at + '\n' + 'Message:
					// ' + chat.message);

				} else {
					alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					callback(0);
				}
			});

		}


		entryButton.addEventListener('click', function() {

			entryButton.title = "...";

			sendMessage(entryField.value, ids, function() {

				entryButton.title = "Send";

			});

			entryField.value = "";

		});

	});

	Ti.App.addEventListener('click', function() {

		self.close();

	});

	function getPushTokens(ids, callback) {

		Cloud.Users.show({
			user_id: friendId
		}, function(e) {
			if (e.success) {

				var user = e.users[0];

				Cloud.PushNotifications.notifyTokens({
					channel: 'wingit',
					to_tokens: user.custom_fields.pushMe,
					payload: {
						"alert": Ti.App.Properties.getString("name").split(" ")[0] + " became your Wing!",
						"c": "confirm",
						"f": Ti.App.Properties.getString("fbid"),
						"n": Ti.App.Properties.getString("name").split(" ")[0],
						"id": Ti.App.Properties.getString("currentUserId")
					}
				}, function(e) {
					if (e.success) {
						alert('Confirmation has been sent!');
						self.close();
					} else {
						alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					}
				});

			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

	}

	return self;
}

module.exports = MessageScreen;
