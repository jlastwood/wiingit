var Globals = require("/lib/Globals");

function Header(titleString, option, win) {

	var upperHeader = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: 96 / 2,
		backgroundColor: 'white',
		top: 30
	});

	if (option == 2) {


		var msgButton = Ti.UI.createButton({
			backgroundImage: '/img/editButton.png',
			width: 126 / 2,
			height: 96 / 2,
			right: 0,
			top: 0
		});

		msgButton.addEventListener('click', function() {

			var MyPicturesScreen = require("/ui/android/MyPicturesScreen");

			new MyPicturesScreen().open();

		});

	} else {

		var msgButton = Ti.UI.createButton({
			backgroundImage: '/img/msgButton.png',
			width: 126 / 2,
			height: 96 / 2,
			right: 0,
			top: 0
		});

		msgButton.addEventListener('click', function() {

			var MessageScreen = require("/ui/android/MyMatchesScreen");

			new MessageScreen().open();

		});

	}

	if (option!==3) {upperHeader.add(msgButton);}

	if (option > 0) {

		var menuButton = Ti.UI.createButton({
			backgroundImage: '/img/backButton.png',
			width: 126 / 2,
			height: 96 / 2,
			left: 0,
			top: 0
		});

		menuButton.addEventListener('click', function() {

			win.close();

		});

	} else {

		var menuButton = Ti.UI.createButton({
			backgroundImage: '/img/menuButton.png',
			width: 126 / 2,
			height: 96 / 2,
			left: 0,
			top: 0
		});

		menuButton.addEventListener('click', function() {

			var SettingsScreen = require("/ui/android/SettingsScreen");

			new SettingsScreen().open();

		});

	}

	upperHeader.add(menuButton);

	if (titleString) {

		var title = Ti.UI.createLabel({
			text: titleString,
			color: "#4dc8f4",
			font: {
				fontSize: 28,
				fontWeight: 'bold'
			}
		});

	} else {

		var title = Ti.UI.createView({
			backgroundImage: "/img/wingitTopLogo.png",
			width: 344 / 2,
			height: 96 / 2
		});
	}

	title.addEventListener("click", function() {

		//upperHeader.parent.close();

	});

	upperHeader.add(title);

	return upperHeader;
}

module.exports = Header;
