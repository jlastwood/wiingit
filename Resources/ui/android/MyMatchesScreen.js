var Globals = require("/lib/Globals");

var Header = require("/ui/android/Header");

function MyMatchesScreen() {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: "white",
		layout: 'vertical'
	});

	var upperHeader = new Header("My Matches", 3, self);

	self.add(upperHeader);

	var scrollView = Ti.UI.createScrollView({
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		layout: 'vertical'
	});

	self.add(scrollView);

	getLikedList(function(list) {

		//alert(list);
		
		Ti.API.info(JSON.stringify(list));

		for (index in list) {
			
			Ti.API.info("MATCH");

			new getUser(list[index], index, function(result) {
				
				Ti.API.info(JSON.stringify(result));
				
				Ti.API.info(JSON.stringify(result.user.id)+" -"+result.photo);

				var view = Ti.UI.createView({
					width: Ti.UI.FILL,
					height: 200,
					borderRadius: 0,
					backgroundColor: '#fff',
					theId: result.user.id
				});

				var leftPhoto = Ti.UI.createImageView({
					width: 160,
					left: 0,
					image: result.photo
				});

				var rightPhoto = Ti.UI.createImageView({
					width: 160,
					right: 0,
					image: result.wingphoto
				});

				view.add(leftPhoto);
				view.add(rightPhoto);

				var deleteButton = Ti.UI.createButton({
					backgroundColor: "red",
					tintColor: "white",
					title: "Delete",
					width: 160,
					height: 200,
					right: 0,
					zIndex: 50,
					theId: result.user.id,

				});

				deleteButton.addEventListener("click", function(e) {

					view.animate({
						opacity: 0.3,
						duration: 200
					});

					//alert(e.source.theId);

					Cloud.Objects.query({
						classname: 'favorites',
						page: 1,
						per_page: 100,
						where: {
							liker: {
								"$in": [Ti.App.Properties.getString('currentUserId'), Ti.App.Properties.getString('currentWing')]
							},
							liked: e.source.theId
						}
					}, function(e) {

						//alert(e);

						var id_array = [];

						for (i in e.favorites) {

							id_array.push(e.favorites[i].id);
						}

						var ids = id_array.join(",");

						Cloud.Objects.remove({
							classname: 'favorites',
							ids: ids
						}, function(e) {
							if (e.success) {

								scrollView.remove(view);
								//alert('Removed');
							} else {
								view.animate({
									opacity: 1,
									duration: 200
								});
								alert('Failed to delete - please try again');
							}
						});

					});

					//view.remove(deleteButton);
				});

				view.addEventListener("click", function(e) {
					view.remove(deleteButton);

					if (e.source.title === undefined) {

						var MessageScreen = require("/ui/android/MessageScreen");

						new MessageScreen(result.user.id, result.wing).open();

					}

				});

				view.addEventListener("swipe", function(e) {

					if (e.direction == "left") {

						view.add(deleteButton);

					}

					if (e.direction == "right") {

						view.remove(deleteButton);

					}

				});

				scrollView.add(view);

			});

		}

	});

	// helper functions

	function getLikedList(callback) {

		Cloud.Objects.query({
			classname: 'favorites',
			page: 1,
			per_page: 10,
			where: {
				liker: {
					"$in": [Ti.App.Properties.getString('currentUserId'), Ti.App.Properties.getString('currentWing')]
				}
			}
		}, function(e) {
			
			Ti.API.info(JSON.stringify(e));

			var answer = [];

			function onlyUnique(value, index, s) {
				return s.indexOf(value) === index;
			}

			for (i in e.favorites) {


				Ti.API.info("ping");
				Ti.API.info(JSON.stringify(e.favorites[i]));
				answer.push(e.favorites[i].liked);

			}
			
			Ti.API.info(answer);

			callback(answer.filter(onlyUnique));

		});

	};

	function getWing(userid, callback) {

		Cloud.Friends.search({
			user_id: userid
		}, function(e) {
			if (e.success) {

				wing = null;

				if (e.users[0]) {
					wing = e.users[0];
				}

				//alert(wing);

				callback(wing);

			} else {
				alert('Error:\\n' + ((e.error && e.message) || JSON.stringify(e)));
				activityIndicator.hide();
			}
		});

	}

	function getProfilePhoto(userid, callback) {

		Cloud.Photos.query({
			page: 1,
			per_page: 10,
			where: {
				user_id: userid,
				tags_array: "profile"
			}
		}, function(e) {
			if (e.success) {

				var photo = "";

				if (e.photos[0].urls.medium_640) {
					photo = e.photos[0].urls.medium_640;
				}

				//alert(photo);

				callback(photo);

			}

		});

	}

	function getUser(userid, index, callback) {

		Cloud.Users.query({
			page: 1,
			per_page: 1,
			where: {
				id: userid
			}
		}, function(e) {

			if (e.success) {

				var reply = {};

				reply.index = index;

				reply.user = e.users[0];

				//alert("userid" + reply.user.id);

				reply.total = e.meta.total_results;

				getWing(e.users[0].id, function(f) {

					//alert("wing" + f);

					if (f == null) {

						reply.wing = null;

						//alert("no wing");

						callback(reply);

					} else {

						reply.wing = f.id;

						getProfilePhoto(reply.user.id, function(g) {

							//alert("myphoto" + g);

							reply.photo = g;

							getProfilePhoto(reply.wing, function(h) {

								//alert("wingphoto" + h);

								reply.wingphoto = h;

								callback(reply);

							});

						});
					}

				});

			} else {

				if (e.message.indexOf("Out of range") !== -1) {

					alert("No more users");

				}

			}
		});

	}

	return self;
}

module.exports = MyMatchesScreen;
