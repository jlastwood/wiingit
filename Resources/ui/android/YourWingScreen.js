var Globals = require("/lib/Globals");

var Header = require("/ui/android/Header");

function YourWingScreen(friendsFromFacebook) {
	

	//alert(Ti.App.Properties.getObject("faceFriends"));

	var loadRemoteImage = function(url, callback) {
		var xhr = Titanium.Network.createHTTPClient();

		xhr.onload = function() {
			// Ti.API.info('image data='+this.responseData);
			callback(this.responseData);

		};
		// open the client
		xhr.open('GET', url);

		// send the data
		xhr.send();
	};

	if (friendsFromFacebook == null || friendsFromFacebook == undefined) {

		var friendsFromFacebook = Ti.App.Properties.getObject('faceFriends');

	}

	var self = Ti.UI.createWindow({
		navBarHidden : true,
		backgroundColor : 'white',
		layout : 'vertical'
	});

	self.addEventListener('open', function() {

		var dialog = Ti.UI.createAlertDialog({
			message : 'Scroll through the suggested friends or type in their name to find your friend',
			ok : 'OK',
			title : 'Choose a friend to join you on a double date!'
		}).show();

	});

	var upperHeader = new Header("Choose Wiing");

	self.add(upperHeader);

	var lowerHeader = Ti.UI.createView({
		width : Ti.UI.FILL,
		height : 96 / 2,
		backgroundColor : 'white',
		top : 20
	});

	var search = Titanium.UI.createSearchBar({
		backgroundColor : 'white',
		barColor : '#fff',
		color : '#4dc8f4',
		borderColor : Globals.is7() ? '#4dc8f4' : "transparent",
		borderWidth : 2,
		height : 43,
		left : 20,
		right : 20,
		top : 0,
		showCancel: false
	});

	search.addEventListener('change', function(e) {

		listView.searchText = e.source.value;

		if (e.source.value.length == 0) {

			e.source.blur();
		}

	});

	search.addEventListener('return', function(e) {

		e.source.blur();

	});

	search.addEventListener('cancel', function(e) {

		e.source.blur();

	});

	lowerHeader.add(search);

	self.add(lowerHeader);

	var myTemplate = {
		properties : {
			height : '90dp'

		},
		childTemplates : [{

			type : 'Ti.UI.View',
			bindId : 'bg',
			properties : {
				width : Ti.UI.FILL,
				height : '80dp',
				left : '20dp',
				right : '20dp',
				top : '10dp',
				backgroundColor : '#def5fc',
					backgroundImage : '/img/bgrow.png'
			}

		}, {
			type : 'Ti.UI.ImageView',
			bindId : 'photo',
			properties : {
				//preventDefaultImage: true,
				width : '80dp',
				height : '80dp',
				left : '20dp',
				top : '10dp',
			}
		}, {// Title
			type : 'Ti.UI.Label',
			bindId : 'name',
			properties : {
				color : '#339bc0',
				font : {
					fontSize : '12dp',
				},
				left : '110dp',
				right : '20dp',
				bottom : '20dp',
			}
		}]
	};

	var listView = Ti.UI.createListView({
		templates : {
			'template' : myTemplate
		},
		defaultItemTemplate : 'template',
		separatorColor : 'transparent',
		separatorInsets : 0,
		backgroundColor: 'white'
	});

	var sections = [];

	var friendSection = Ti.UI.createListSection({
		backgroundColor:"white",
		height:0
	});
	var friendDataSet = [];

	for (i in friendsFromFacebook) {

		var age;

		if (friendsFromFacebook[i].birthday) {

			var birthYear = parseInt(friendsFromFacebook[i].birthday.split("/")[2]);

			age = birthYear > 0 ? new Date().getFullYear() - birthYear : "";

		} else {

			age = "";

		}

		var image;

		var singleFriend = {
			name : {
				text : friendsFromFacebook[i].name + "\n" + age
			},
			photo : {
				image : "https://graph.facebook.com/" + friendsFromFacebook[i].id + "/picture?width=128&height=128&redirect=1"
			},
			properties : {
				searchableText : friendsFromFacebook[i].name,
				fbid : friendsFromFacebook[i].id,
				username : friendsFromFacebook[i].username,
				gender : friendsFromFacebook[i].gender
			}
		};

		// only populate with same-gender friends

		if (Ti.App.Properties.getString("gender") == friendsFromFacebook[i].gender) {

			friendDataSet.push(singleFriend);
			//Ti.API.info(singleFriend);

		}

	}

	//Ti.API.info("elo" + JSON.stringify(friendDataSet));
	
	Ti.API.info("STATUS: Wing Selection Screen");

	friendSection.setItems(friendDataSet);
	sections.push(friendSection);

	listView.setSections(sections);

	listView.addEventListener('itemclick', function(e) {

		var item = friendSection.getItemAt(e.itemIndex);

		var userName = item.properties.searchableText.split(" ")[0];
		var fbid = item.properties.fbid;
		var uname = item.properties.username;

		var gen = item.properties.gender == "male" ? "him" : "her";

		var dialog = Ti.UI.createAlertDialog({
			message : "A notification will be sent to " + userName + " via the Wiingit app (or Facebook if he does not have the app installed), letting " + gen + " know you want " + gen + " to be your Wiing.",
			cancel : 0,
			buttonNames : ["Cancel", 'Send Invite'],
			title : "Are you sure you want to ask " + userName + " to be your Wiing?"
		});

		dialog.addEventListener('click', function(f) {
			if (f.index === 1) {
				Ti.API.info('OK clicked');

				Cloud.Users.query({
					page : 1,
					per_page : 10,
					where : {
						facebookId : fbid
					}
				}, function(e) {
					if (e.success) {

						if (e.users.length == 0) {

							// email
							var emailDialog = Ti.UI.createEmailDialog();
							emailDialog.subject = "Become my Wing on WingIt!";
							emailDialog.toRecipients = [uname + "@facebook.com"];
							emailDialog.messageBody = 'Here go links to our app on AppStore / Google Play';
							emailDialog.open();

						} else {

							var user = e.users[0];
							//alert('id: ' + user.id + '\n' + 'first name: ' + user.first_name + '\n' + 'last
							// name: ' + user.last_name);

							Cloud.Friends.add({
								user_ids : user.id,
							}, function(e) {
								if (e.success) {
									Ti.App.Properties.setString("currentWing", user.id);
								} else {
									alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
								}
							});

							Cloud.PushNotifications.notifyTokens({
								channel : 'wingit',
								to_tokens : user.custom_fields.pushMe,
								payload : {
									"alert" : "Become my Wing on WingIt!",
									"c" : "invite",
									"f" : Ti.App.Properties.getString("fbid"),
									"n" : Ti.App.Properties.getString("name").split(" ")[0],
									"id" : Ti.App.Properties.getString("currentUserId")
								}
							}, function(e) {
								if (e.success) {



											var dialog = Ti.UI.createAlertDialog({
												message : 'Invitation has been sent. You will be notified when the person agrees to become your Wiing. Once your Wiing accepts you will receive a pop up notification, then you will be able to begin browsing for matches',
												buttonNames: ['OK'],
												title: "Invitation sent"
											});
											
											dialog.show();
	

								} else {
									alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
								}
							});

						}

					} else {
						alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					}
				});

			}

		});

		dialog.show();

	});

	self.add(listView);

	Ti.App.addEventListener('logMeOut', function() {

		self.close();

	});

	return self;
}

module.exports = YourWingScreen;
