var Globals = require("/lib/Globals");

var Header = require("/ui/android/Header");

function MyProfileScreen(friendName) {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
	});

	var upperHeader = new Header("My Profile", 2, self);

	self.add(upperHeader);

	var outerScroll = Ti.UI.createView({
		top: 100,
		width: Ti.UI.FILL,
		height: Ti.UI.FILL
	});

	var theViews = [Ti.UI.createView()];

	var photosView = Ti.UI.createScrollableView({
		backgroundColor: 'white',
		showPagingControl: false,
		views:theViews,
		//views: [img1, img2],
		width: Ti.UI.FILL,
		height: Ti.UI.SIZE,
		top: 0
	});

	outerScroll.add(photosView);
	
	

	var nameText = Ti.UI.createLabel({
		text: Ti.App.Properties.getString('name').split(" ")[0],
		color: "#4dc8f4",
		font: {
			fontSize: 28,
			fontWeight: 'bold'
		},
		top: 210,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
	});

	outerScroll.add(nameText);

	var ageText = Ti.UI.createLabel({
		text: "",
		color: "#999",
		font: {
			fontSize: 14
		},
		top: 250
	});

	outerScroll.add(ageText);

	var comparator;

	var textArea = Ti.UI.createTextArea({
		editable: true,
		color: '#999',
		font: {
			fontSize: 14,
		},
		textAlign: 'left',
		value: '',
		hintText: "Enter your description here",
		top: 300,
		left: 20,
		right: 20,
		height: Titanium.Platform.displayCaps.platformHeight == 568 ? 120 : 50
	});

	outerScroll.add(textArea);

	self.add(outerScroll);
	
	
	

	Ti.App.addEventListener('logMeOut', function() {

		self.close();

	});

	// get basic data

	Cloud.Users.showMe(function(e) {
		if (e.success) {

			var user = e.users[0];

			var birthday = user.custom_fields.born;

			var birthYear = parseInt(birthday.split("/")[2]);

			var ageValue = birthYear > 0 ? new Date().getFullYear() - birthYear : "";

			ageText.text = (ageValue == "") ? "" : "AGE " + ageValue;

			textArea.value = user.custom_fields.description ? user.custom_fields.description : "";

			comparator = textArea.value;

		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});

	// save on leaving screen

	self.addEventListener('blur', function() {

		if (textArea.value != comparator) {

			Cloud.Users.update({
				custom_fields: {
					description: textArea.value
				}

			}, function(g) {
				if (g.success) {

					comparator = textArea.value;

				} else {
					alert('UPD Error:\n' + ((g.error && g.message) || JSON.stringify(g)));
				}
			});

		}

	});

	// get profile photo

	Cloud.Photos.query({
		page: 1,
		per_page: 20,
		where: {
			//tags_array: "profile",
			user_id: Ti.App.Properties.getString('currentUserId')
		}
	}, function(e) {
		if (e.success) {

			//	alert(e.photos.length);

			var temp = [];

			for (var i = 0; i < e.photos.length; i++) {
				var photo = e.photos[i];

				var img = Ti.UI.createImageView({
					image: photo.urls.medium_640,
					height: 190,
					preventDefaultImage: true
				});

				photo.tags && photo.tags.indexOf("profile") !== -1 ? theViews.push(img) : temp.push(img);

			}

			theViews = theViews.concat(temp);

			photosView.views = theViews;

		} else {
			alert('UPL Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});

	Ti.App.addEventListener('updateProfilePhoto', function() {
		
		theViews=[];

		Cloud.Photos.query({
			page: 1,
			per_page: 20,
			where: {
				//tags_array: "profile",
				user_id: Ti.App.Properties.getString('currentUserId')
			}
		}, function(e) {
			if (e.success) {

				//	alert(e.photos.length);

				var temp = [];

				for (var i = 0; i < e.photos.length; i++) {
					var photo = e.photos[i];

					var img = Ti.UI.createImageView({
						image: photo.urls.medium_640,
						height: 190,
						preventDefaultImage: true
					});

					photo.tags && photo.tags.indexOf("profile") !== -1 ? theViews.push(img) : temp.push(img);

				}

				theViews = theViews.concat(temp);

				photosView.views = theViews;

			} else {
				alert('UPL Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

	});

	return self;
}

module.exports = MyProfileScreen;
