var Globals = require("/lib/Globals");

function ChatElements() {

	var last_update_time;
	

	var self = {};
	
	self.getUpdateTime = function(){
		
		return last_update_time;
	};
	
	self.setUpdateTime = function(val){
		
		last_update_time=val;
		
	};

	var grayBubble = function(userId, userMessage, userPhoto) {

		var grayAround = Ti.UI.createView({
			top: 20,
			height: Ti.UI.SIZE
		});

		var grayBubble = Ti.UI.createView({
			backgroundImage: '/img/grayBubble.png',
			width: 220,
			height: Ti.UI.SIZE,
			bottom: 20,
			right: 60,
			backgroundLeftCap: 20,
			backgroundRightCap: 20,
			backgroundTopCap: 20,
			backgroundBottomCap: 20,
			layout: 'vertical'
		});

		var grayPhoto = Ti.UI.createImageView({
			image: userPhoto,
			preventDefaultImage: true,
			backgroundColor: "#eee",
			width: 40,
			height: 40,
			right: 10,
			bottom: 0,
			borderRadius: 20
		});

		var grayText = Ti.UI.createLabel({
			left: 20,
			right: 20,
			top: 20,
			text: userMessage,
			color: '#999'
		});

		grayBubble.add(grayText);

		var margin = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: 20
		});

		grayBubble.add(margin);

		grayAround.add(grayBubble);
		grayAround.add(grayPhoto);

		return grayAround;

	};

	self.grayBubble = grayBubble;

	var blueBubble = function(userId, userMessage, userPhoto) {

		var blueAround = Ti.UI.createView({
			top: 20,
			height: Ti.UI.SIZE
		});

		var blueBubble = Ti.UI.createView({
			backgroundImage: '/img/blueBubble.png',
			width: 220,
			height: Ti.UI.SIZE,
			bottom: 20,
			left: 60,
			backgroundLeftCap: 20,
			backgroundRightCap: 20,
			backgroundTopCap: 20,
			backgroundBottomCap: 20,
			layout: 'vertical'
		});

		var blueText = Ti.UI.createLabel({
			left: 26,
			right: 10,
			top: 10,
			text: userMessage,
			color: '#fff'
		});

		blueBubble.add(blueText);

		var bluemargin = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: 14
		});

		blueBubble.add(bluemargin);

		var bluePhoto = Ti.UI.createImageView({
			image: userPhoto,
			preventDefaultImage: true,
			backgroundColor: "#eee",
			width: 40,
			height: 40,
			left: 10,
			bottom: 0,
			borderRadius: 20
		});

		blueAround.add(bluePhoto);
		blueAround.add(blueBubble);

		return blueAround;

	};

	self.blueBubble = blueBubble;

	self.getAllPhotos = function(p1, p2, p3, p4, callback) {

		function getProfilePhoto(userId, callback) {

			Cloud.Photos.query({
				page: 1,
				per_page: 20,
				where: {
					tags_array: "profile",
					user_id: userId
				}
			}, function(e) {
				if (e.success && e.meta.total_results > 0) {

					if (e.photos[0].urls) {

						callback(e.photos[0].urls.square_75);

					} else {

						callback("");
					}

				} else {

					callback("");
					//alert('UPL Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
				}
			});

		};

		var reply = {};

		getProfilePhoto(p1, function(rp1) {

			reply.me = rp1;

			getProfilePhoto(p2, function(rp2) {

				reply.wing = rp2;

				getProfilePhoto(p3, function(rp3) {

					reply.match1 = rp3;

					getProfilePhoto(p4, function(rp4) {

						reply.match2 = rp4;

						callback(reply);

					});

				});

			});

		});

	};

	self.getMessagesSince = function(users, timestamp, callback) {
		
		Ti.API.info(users);
		
		for (i in users) {
			
			if (users[i]==null) {
				Ti.API.info("cut");
				
				users.splice(i, 1);
			}
		}

		Ti.API.info(users);

		Cloud.Chats.query({
			participate_ids: users.join(','),
			where: {
				updated_at: {
					'$gt': timestamp
				}
			},
			
			order: "-updated_at"
		}, function(e) {
			if (e.success) {

				//alert(timestamp);

				callback(e);

			} else {
				
				var v = users.join("::");
				
				//alert("youuu");
				alert('Error:' + ((e.error && e.message) || JSON.stringify(e)) + "--" +v);
			}
		});

	};

	self.putMessages = function(messages, arena, thePhotos, ids) {

		for (var i = messages.chats.length - 1; i > -1; i--) {
			var chat = messages.chats[i];

			//alert(e);

			if (chat.from.id == Ti.App.Properties.getString('currentUserId')) {

				arena.add(new blueBubble(Ti.App.Properties.getString('currentUserId'), chat.message, thePhotos.me));

			} else if (chat.from.id == Ti.App.Properties.getString('currentWing')) {

				arena.add(new grayBubble(Ti.App.Properties.getString('currentWing'), chat.message, thePhotos.wing));

			} else if (chat.from.id == ids[2]) {
				
				arena.add(new grayBubble(ids[2], chat.message, thePhotos.match1));
				
			} else if (chat.from.id == ids[3]) {
				
				arena.add(new grayBubble(ids[3], chat.message, thePhotos.match2));	
				
			}	

			setTimeout(function() {

				arena.scrollToBottom();

			}, 300);

			self.setUpdateTime(chat.updated_at);

		}

		//alert('Success:\n' + 'From: ' + chat.from.first_name + ' ' +
		// chat.from.last_name + '\n' + 'Updated: ' + chat.updated_at + '\n' + 'Message:
		// ' + chat.message);

	};
	
	

	

	return self;
}

module.exports = ChatElements;

