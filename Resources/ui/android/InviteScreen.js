var Globals = require("/lib/Globals");

var Header = require("/ui/android/Header");

var UnwingScreen = require("/ui/android/UnwingScreen");
var ProfileBrowserScreen = require("/ui/android/ProfileBrowserScreen");

var WingMe = require("/connections/WingMe");
var GetUserFromCloud = require("/connections/GetUserFromCloud");
var SendPushToToken = require("/connections/SendPushToToken");

var Cloud = require('ti.cloud');

function InviteScreen(friendId, friendName, friendFbid) {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
	});

	var upperHeader = new Header();

	self.add(upperHeader);

	var photos = Ti.UI.createView({
		width: Ti.UI.FILL,
		top: 80
	});

	var userImage = Ti.UI.createImageView({
		image: "http://graph.facebook.com/" + friendFbid + "/picture?width=256&height=256",
		top: 30,
		left: 20,
		width: 128,
		height: 128,
		preventDefaultImage: true
	});

	var myImage = Ti.UI.createImageView({
		image: "http://graph.facebook.com/" + Ti.App.Properties.getString('fbid') + "/picture?width=256&height=256",
		top: 30,
		right: 20,
		width: 128,
		height: 128,
		preventDefaultImage: true
	});

	photos.add(userImage);
	photos.add(myImage);

	self.add(photos);

	var angelsBg = Ti.UI.createImageView({
		image: "/img/angelsAccepted.png",
		width: (440 / 2) * (Titanium.Platform.displayCaps.platformHeight == 576 ? 1 : .7653),
		height: (280 / 2) * (Titanium.Platform.displayCaps.platformHeight == 576 ? 1 : .7653),
		bottom: 100 / 2,
		backgroundColor: 'yellow'
	});

	self.add(angelsBg);

	var acceptText = Ti.UI.createLabel({
		text: friendName + " invited you to Wing",
		color: "#4dc8f4",
		font: {
			fontSize: 28,
			fontWeight: 'bold'
		},
		top: 260,
		left: 10,
		right: 10,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
	});

	self.add(acceptText);

	var declineButton = Ti.UI.createButton({
		bottom: 10,
		left: 20,
		title: "Decline",
		color: "#4dc8f4",
		font: {
			fontSize: 18,
			fontWeight: 'bold'
		},
		backgroundColor:'white'
	});

	self.add(declineButton);

	var acceptButton = Ti.UI.createButton({
		bottom: 10,
		right: 20,
		title: "Accept",
		color: "#4dc8f4",
		font: {
			fontSize: 18,
			fontWeight: 'bold'
		},
		backgroundColor:'white'
	});

	self.add(acceptButton);

	// controllers

	acceptButton.addEventListener('click', function() {

		// set current wing link

		Ti.API.info("GOT CLICK");

		Cloud.Friends.approve({
			user_ids: friendId
		}, function(e) {
			if (e.success) {
				Ti.App.Properties.setString("currentWing", friendId);

				Ti.API.info('YEAH!!!!');

				new WingMe({
					user1: Ti.App.Properties.getString("currentUserId"),
					user2: friendId
				}, function() {

					Ti.API.info("DIDIT");
					
					new ProfileBrowserScreen().open();
					self.close();

				});

			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

		// send push notification to Wing to notify acceptation

		new GetUserFromCloud(friendId, function(user) {

			var payload = {
				"alert": Ti.App.Properties.getString("name").split(" ")[0] + " became your Wing!",
				"c": "confirm",
				"f": Ti.App.Properties.getString("fbid"),
				"n": Ti.App.Properties.getString("name").split(" ")[0],
				"id": Ti.App.Properties.getString("currentUserId")
			};

			new SendPushToToken(user.custom_fields.pushMe, payload, function(){
				
				alert('Confirmation has been sent!');
					
				
			});


		});

		// show date browser - TBI

		

	});

	declineButton.addEventListener('click', function() {

		// send push notification to user about decline

		new GetUserFromCloud(friendId, function(user) {

			Cloud.PushNotifications.notifyTokens({
				channel: 'wingit',
				to_tokens: user.custom_fields.pushMe,
				payload: {
					"alert": Ti.App.Properties.getString("name").split(" ")[0] + " declined to be your Wing...",
					"c": "decline",
					"f": Ti.App.Properties.getString("fbid"),
					"n": Ti.App.Properties.getString("name").split(" ")[0],
					"id": Ti.App.Properties.getString("currentUserId")
				}
			}, function(e) {
				if (e.success) {
					alert('Decline has been sent!');
					self.close();
				} else {
					alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
				}
			});

		});

		self.close();

	});

	Ti.App.addEventListener('logMeOut', function() {

		self.close();

	});

	return self;
}

module.exports = InviteScreen;
