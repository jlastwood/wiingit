var Globals = require("/lib/Globals");

var Header = require("/ui/iphone/Header");

var UnwingScreen = require("/ui/iphone/UnwingScreen");
var ProfileBrowserScreen = require("/ui/iphone/ProfileBrowserScreen");

var Cloud = require('ti.cloud');

function InviteScreen(friendId, friendName, friendFbid) {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
	});

	var upperHeader = new Header();

	self.add(upperHeader);

	var photos = Ti.UI.createView({
		width: Ti.UI.FILL,
		top: 80
	});

	var userImage = Ti.UI.createImageView({
		image: "http://graph.facebook.com/" + friendFbid + "/picture?width=256&height=256",
		top: 30,
		left: 20,
		width: 128,
		height: 128,
		preventDefaultImage: true
	});

	var myImage = Ti.UI.createImageView({
		image: "http://graph.facebook.com/" + Ti.App.Properties.getString('fbid') + "/picture?width=256&height=256",
		top: 30,
		right: 20,
		width: 128,
		height: 128,
		preventDefaultImage: true
	});

	photos.add(userImage);
	photos.add(myImage);

	self.add(photos);

	var angelsBg = Ti.UI.createImageView({
		image: "/img/angelsAccepted.png",
		width: (440 / 2) * (Titanium.Platform.displayCaps.platformHeight == 576 ? 1 : .7653),
		height: (280 / 2) * (Titanium.Platform.displayCaps.platformHeight == 576 ? 1 : .7653),
		bottom: 100 / 2,
		backgroundColor: 'yellow'
	});

	self.add(angelsBg);

	var acceptText = Ti.UI.createLabel({
		text: friendName + " invited you to be her WIING",
		color: "#4dc8f4",
		font: {
			fontSize: 28,
			fontWeight: 'bold'
		},
		top: 260,
		left: 10,
		right: 10,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
	});

	self.add(acceptText);

	var declineButton = Ti.UI.createButton({
		bottom: 10,
		left: 20,
		title: "Decline",
		color: "#4dc8f4",
		font: {
			fontSize: 18,
			fontWeight: 'bold'
		},
		style: Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});

	self.add(declineButton);

	var acceptButton = Ti.UI.createButton({
		bottom: 10,
		right: 20,
		title: "Accept",
		color: "#4dc8f4",
		font: {
			fontSize: 18,
			fontWeight: 'bold'
		},
		style: Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});

	self.add(acceptButton);

	// controllers

	acceptButton.addEventListener('click', function() {

		// set current wing link

		Cloud.Friends.approve({
			user_ids: friendId
		}, function(e) {
			if (e.success) {
				Ti.App.Properties.setString("currentWing", friendId);
			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});


		// send push notification to Wing to notify acceptation

		Cloud.Users.show({
			user_id: friendId
		}, function(e) {
			if (e.success) {

				var user = e.users[0];

				Cloud.PushNotifications.notifyTokens({
					channel: 'wingit',
					to_tokens: user.custom_fields.pushMe,
					payload: {
						"alert": Ti.App.Properties.getString("name").split(" ")[0] + " became your Wiing!",
						"c": "confirm",
						"f": Ti.App.Properties.getString("fbid"),
						"n": Ti.App.Properties.getString("name").split(" ")[0],
						"id": Ti.App.Properties.getString("currentUserId")
					}
				}, function(e) {
					if (e.success) {
						alert('Confirmation has been sent!');
						self.close();
					} else {
						alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					}
				});

			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

		// show date browser - TBI

		new ProfileBrowserScreen().open();

	});

	declineButton.addEventListener('click', function() {

		// send push notification to user about decline

		Cloud.Users.show({
			user_id: friendId
		}, function(e) {
			if (e.success) {
				var user = e.users[0];

				Cloud.PushNotifications.notifyTokens({
					channel: 'wingit',
					to_tokens: user.custom_fields.pushMe,
					payload: {
						"alert": Ti.App.Properties.getString("name").split(" ")[0] + " declined to be your Wiing...",
						"c": "decline",
						"f": Ti.App.Properties.getString("fbid"),
						"n": Ti.App.Properties.getString("name").split(" ")[0],
						"id": Ti.App.Properties.getString("currentUserId")
					}
				}, function(e) {
					if (e.success) {
						alert('Decline has been sent!');
						self.close();
					} else {
						alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					}
				});

			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

		self.close();

	});

	Ti.App.addEventListener('logMeOut', function() {

		self.close();

	});

	return self;
}

module.exports = InviteScreen;
