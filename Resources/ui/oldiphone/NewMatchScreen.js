var Globals = require("/lib/Globals");

function NewMatchScreen() {

	var self = Ti.UI.createWindow({
		navBarHidden:true
	});
	

	Ti.App.addEventListener('logMeOut', function(){
		
		self.close();
		
	});
	
	return self;
}

module.exports = NewMatchScreen;
