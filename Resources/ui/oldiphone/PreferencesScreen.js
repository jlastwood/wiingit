var Globals = require("/lib/Globals");

var Header = require("/ui/iphone/Header");

var Slider = require("/ui/iphone/Slider");

function PreferencesScreen() {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white'
	});

	self.add(new Header("Preferences", 1, self));

	var likeText = Ti.UI.createLabel({
		text: "We like",
		color: "#a7a9ac",
		font: {
			fontSize: 22
		},
		top: 110,
		left: 40,
		right: 10,
		textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
	});

	var switchGirls = Ti.UI.createView({
		width: 60,
		height: 75,
		top: 90,
		left: 120,
		backgroundImage: "/img/girlsEmpty.png",
		selected: false
	});

	switchGirls.addEventListener('click', function(e) {

		if (e.source.selected == false) {

			e.source.selected = true;
			e.source.backgroundImage = "/img/girlsFull.png";

			switchBoys.selected = false;
			switchBoys.backgroundImage = "/img/boysEmpty.png";

		} else {

			e.source.selected = false;
			e.source.backgroundImage = "/img/girlsEmpty.png";

			switchBoys.selected = true;
			switchBoys.backgroundImage = "/img/boysFull.png";

		}

	});

	var switchBoys = Ti.UI.createView({
		width: 60,
		height: 75,
		top: 90,
		left: 180,
		backgroundImage: "/img/boysEmpty.png",
		selected: false
	});

	switchBoys.addEventListener('click', function(e) {

		if (e.source.selected == false) {

			e.source.selected = true;
			e.source.backgroundImage = "/img/boysFull.png";

			switchGirls.selected = false;
			switchGirls.backgroundImage = "/img/girlsEmpty.png";

		} else {

			e.source.selected = false;
			e.source.backgroundImage = "/img/boysEmpty.png";

			switchGirls.selected = true;
			switchGirls.backgroundImage = "/img/girlsFull.png";

		}

	});

	self.add(switchBoys);
	self.add(switchGirls);

	self.add(likeText);

	var minText = Ti.UI.createLabel({
		text: "Age Between",
		color: "#a7a9ac",
		font: {
			fontSize: 22
		},
		top: 170,
		left: 40,
		right: 10,
		textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
	});

	self.add(minText);

	var minSlider = new Slider({
		top: 210,
		min: 18,
		max: 50,
		startVal: Ti.App.Properties.getInt('minAge')
	});

	self.add(minSlider);

	var maxSlider = new Slider({
		top: 260,
		min: 18,
		max: 50,
		startVal: Ti.App.Properties.getInt('maxAge')
	});

	self.add(maxSlider);

	var locationText = Ti.UI.createLabel({
		text: "Location",
		color: "#a7a9ac",
		font: {
			fontSize: 22
		},
		top: 330,
		left: 40,
		right: 10,
		textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
	});

	self.add(locationText);

	var locationSlider = new Slider({
		top: 360,
		min: 0,
		max: 100,
		startVal: Ti.App.Properties.getInt('maxDistance')
	});

	self.add(locationSlider);

	var vibrateText = Ti.UI.createLabel({
		text: "Vibrate",
		color: "#a7a9ac",
		font: {
			fontSize: 22
		},
		top: 430,
		left: 40,
		right: 10,
		textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
	});

	self.add(vibrateText);

	var mod = require('ti.sevenswitch');

	var vibrateSwitch = mod.createSwitch({
		top: 430,
		left: 150,
		height: 30,
		value: true,
		onColor: "#49c7f4",
		width: 50
	});

	self.add(vibrateSwitch);

	/*
	 will be enough on 3.3

	 var vibrateSwitch = Ti.UI.createSwitch({
	 top: 430,
	 left: 150,
	 value: true,
	 tintColor: "#49c7f4"
	 });

	 self.add(vibrateSwitch);
	 */
	Ti.App.addEventListener('logMeOut', function() {

		self.close();

	});

	self.addEventListener('focus', function() {


		if (Ti.App.Properties.hasProperty("preferredGender")) {

			if (Ti.App.Properties.getString("preferredGender") == "boys") {

				switchBoys.selected = true;
				switchBoys.backgroundImage = "/img/boysFull.png";

				switchGirls.selected = false;
				switchGirls.backgroundImage = "/img/girlsEmpty.png";

			} else {
				

				switchGirls.selected = true;
				switchGirls.backgroundImage = "/img/girlsFull.png";

				switchBoys.selected = false;
				switchBoys.backgroundImage = "/img/boysEmpty.png";
						
			}

		}

		if (Ti.App.Properties.hasProperty("minAge")) {

			minSlider.setValue(Ti.App.Properties.getInt('minAge'));

		}

		if (Ti.App.Properties.hasProperty("maxAge")) {

			maxSlider.setValue(Ti.App.Properties.getInt('maxAge'));

		}

		if (Ti.App.Properties.hasProperty("maxDistance")) {

			locationSlider.setValue(Ti.App.Properties.getInt('maxDistance'));

		}

		if (switchGirls.backgroundImage == "/img/girlsFull.png") {

			Ti.App.Properties.setString("preferredGender", "girls");

		} else {

			Ti.App.Properties.setString("preferredGender", "boys");
		}

	});

	self.addEventListener('blur', function() {

		if (switchGirls.backgroundImage == "/img/girlsFull.png") {

			Ti.App.Properties.setString("preferredGender", "girls");

		} else {

			Ti.App.Properties.setString("preferredGender", "boys");
		}

		Ti.App.Properties.setInt("minAge", minSlider.value);
		Ti.App.Properties.setInt("maxAge", maxSlider.value);

		Ti.App.Properties.setInt("maxDistance", locationSlider.value);
		
		Ti.App.fireEvent('prefsChanged');

	});

	return self;
}

module.exports = PreferencesScreen;
