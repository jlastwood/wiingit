var Globals = require("/lib/Globals");

var Header = require("/ui/iphone/Header");

var ProfileBrowserScreen = require("/ui/iphone/ProfileBrowserScreen");

var Cloud = require('ti.cloud');

function AcceptedScreen(friendId, friendName, friendFbid) {

	// update current wing (screen called up by push notification from accepting
	// user)

	Ti.App.Properties.setString("currentWing", friendId);

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
	});

	var upperHeader = new Header();

	self.add(upperHeader);

	var photos = Ti.UI.createView({
		width: Ti.UI.FILL,
		top: 80
	});

	var userImage = Ti.UI.createImageView({
		image: "http://graph.facebook.com/" + friendFbid + "/picture?width=256&height=256",
		top: 30,
		left: 20,
		width: 128,
		height: 128,
		preventDefaultImage: true
	});

	var myImage = Ti.UI.createImageView({
		image: "http://graph.facebook.com/" + Ti.App.Properties.getString('fbid') + "/picture?width=256&height=256",
		top: 30,
		right: 20,
		width: 128,
		height: 128,
		preventDefaultImage: true
	});

	photos.add(userImage);
	photos.add(myImage);

	self.add(photos);

	var angelsBg = Ti.UI.createImageView({
		image: "/img/angelsAccepted.png",
		width: (440 / 2) * (Titanium.Platform.displayCaps.platformHeight == 576 ? 1 : .7653),
		height: (280 / 2) * (Titanium.Platform.displayCaps.platformHeight == 576 ? 1 : .7653),
		bottom: 100 / 2,
		backgroundColor: 'yellow'
	});

	self.add(angelsBg);

	var acceptText = Ti.UI.createLabel({
		text: friendName + " accepted your Wing invite!",
		color: "#4dc8f4",
		font: {
			fontSize: 28,
			fontWeight: 'bold'
		},
		top: 260,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER
	});

	self.add(acceptText);

	var getStartedButton = Ti.UI.createButton({
		bottom: 10,
		title: "Get Started",
		color: "#4dc8f4",
		font: {
			fontSize: 18,
			fontWeight: 'bold'
		},
		style: Titanium.UI.iPhone.SystemButtonStyle.PLAIN
	});

	self.add(getStartedButton);

	getStartedButton.addEventListener('click', function() {

				new ProfileBrowserScreen().open();


	});

	Ti.App.addEventListener('logMeOut', function() {

		self.close();

	});

	return self;
}

module.exports = AcceptedScreen;
