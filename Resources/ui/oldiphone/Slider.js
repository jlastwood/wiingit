var Globals = require("/lib/Globals");

function Slider(prefs) {

	var self = Ti.UI.createView({
		top:prefs.top,
		bottom:prefs.bottom,
		left:prefs.left,
		right:prefs.right,
		width:prefs.width,
		height:prefs.height
	});


	var slider = Titanium.UI.createSlider({
		top: 26,
		min: prefs.min,
		max: prefs.max,
		value: prefs.startVal,
		leftTrackImage: "/img/leftTrack.png",
		rightTrackImage: "/img/rightTrack.png",
		thumbImage: '/img/thumb.png',
		left: 40,
		right: 40
	});

	var leftBorder = Ti.UI.createView({
		width:4,
		height:14,
		left:37,
		top:27,
		backgroundImage: '/img/thumb.png'
	});
	
	self.add(leftBorder);

	var rightBorder = Ti.UI.createView({
		width:4,
		height:14,
		right:37,
		top:27,
		backgroundImage: '/img/thumb.png'
	});
	
	self.add(rightBorder);
	
	var ageMinLabel = Ti.UI.createLabel({
		top:0,
		text:"0",
		color:"#a7a9ac"
	});
	
	self.add(ageMinLabel);
	
	
	slider.addEventListener('change', function(e) {
		ageMinLabel.text=parseInt(e.value);
		self.value = parseInt(e.value);
	});	
	
	self.add(slider);
	
	self.value = parseInt(prefs.max/2);
	
	
	self.setValue = function(val) {
		
		sliderValue = val;
	};
		
	
	return self;
}

module.exports = Slider;
