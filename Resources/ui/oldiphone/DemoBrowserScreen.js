var Globals = require("/lib/Globals");

var LoginWithFB = require("/connections/LoginWithFB");

function DemoBrowserScreen() {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white'
	});
	
	var ratio = Titanium.Platform.displayCaps.platformHeight==568 ? 1 : 0.83;

	var ratio2 = Titanium.Platform.displayCaps.platformHeight==568 ? 1 : 0.87;


	var messages = ['Search your Facebook friends to find your WIING', 'Browse local "Wiinged" pairs to find a match you both like', 'Both you and your WIING will be notified when either of you find a new match', 'Start a 4-way conversation and set up a date', 'Seize the opportunity. When either pair "un-wiing" the match is lost'];


	var views = [];

	for ( i = 0; i < 5; i++) {

		views[i] = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: Ti.UI.FILL
		});

		var lab = Ti.UI.createLabel({
			left:20,
			right:20,
			textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
			font: {
				fontSize: 14
			},
			text: messages[i],
			color:"black",
			top: 30
		});

		views[i].add(lab);

		var img = Ti.UI.createView({
			top:70,
			width: 600 / 2 * ratio, 
			height: 776 / 2 * ratio,
			backgroundImage: '/img/demo'+(1+i)+'.png'
		});

		views[i].add(img);

	}

	var scrollableView = Ti.UI.createScrollableView({
		views: views,
		showPagingControl: true,
		top: 0,
		width: 640 / 2,
		height: 960/2 * ratio2,
		pagingControlColor: 'transparent',
		backgroundColor: 'transparent'
	});

	self.add(scrollableView);
	
	var fbButton = Ti.UI.createButton({
		width: 251 * ratio,
		height: 50 * ratio,
		bottom: 30 * ratio,
		backgroundImage: '/img/fbButton.png'
	});

	self.add(fbButton);
	


	var clicked = 0;


	fbButton.addEventListener('click', function() {

		Ti.API.info('yo');

		clicked = 0;

		new LoginWithFB(function(e) {

			var YourWingScreen = require("/ui/iphone/YourWingScreen");

			new YourWingScreen(e).open();

		});


	});




	Ti.App.addEventListener('logMeOut', function(){
		
		self.close();
		
	});


	return self;
}

module.exports = DemoBrowserScreen; 