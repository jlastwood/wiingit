var Globals = require("/lib/Globals");

var LoginWithFB = require("/connections/LoginWithFB");

function LandingScreen() {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white'
	});

	var logo = Ti.UI.createView({
		width: 640 / 2,
		height: 400 / 2,
		backgroundImage: '/img/landingLogo.png'
	});

	self.add(logo);

	var fbButton = Ti.UI.createButton({
		width: 251,
		height: 50,
		bottom: 50,
		backgroundImage: '/img/fbButton.png'
	});

	self.add(fbButton);

	var fbLabel = Ti.UI.createLabel({
		width: 251,
		bottom: 20,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		font: {fontSize:11},
		color:'#999',
		text: 'Wiingit will never post to your Facebook'
	});

	self.add(fbLabel);



	// -CONTROLLERS- //

	var clicked = 0;

	fbButton.addEventListener('click', function() {

		Ti.API.info('yo');

		clicked = 0;

		new LoginWithFB(function(e) {

			var YourWingScreen = require("/ui/iphone/YourWingScreen");

			new YourWingScreen(e).open();

		});

	});

	Ti.App.addEventListener('logMeOut', function() {

		self.close();

	});

	return self;
}

module.exports = LandingScreen;

