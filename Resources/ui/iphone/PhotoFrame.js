var Globals = require("/lib/Globals");

function PhotoFrame(prefs) {

	var frame = Ti.UI.createView({
		width: prefs.theSize,
		height: prefs.theSize,
		top: prefs.top,
		left: prefs.left,
		right: prefs.right
	});

	frame.addPhoto = function(photo, id) {

		empty.setImage(photo);
		empty.cloudPhotoId = id;

		picAdd.removeEventListener('click', addPiC);
		picAdd.backgroundImage = '/img/picDel.png';
		picAdd.addEventListener('click', removePiC);
	};

	frame.clear = function() {

		empty.setImage();
		empty.cloudPhotoId = null;
	};

	var picAdd = Ti.UI.createButton({
		width: 36 * .66,
		height: 36 * .66,
		backgroundImage: "/img/picAdd.png",
		top: 0,
		right: 0,
		zIndex: 5
	});

	var empty = Ti.UI.createImageView({
		left: 10,
		right: 10,
		top: 10,
		bottom: 10,
		backgroundColor: "#fff",
		borderColor: "#4dc8f4",
		borderWidth: 1,
		preventDefaultImage: true,
		zIndex: 1
	});

	empty.addEventListener('click', function(u) {

		//alert(u.source.cloudPhotoId);
	});

	frame.add(empty);
	frame.add(picAdd);

	function removePiC() {

		//alert(empty.cloudPhotoId);
		picAdd.removeEventListener('click', removePiC);

		Cloud.Photos.remove({
			photo_id: empty.cloudPhotoId
		}, function(e) {
			if (e.success) {
				Ti.App.fireEvent('updateProfilePhoto');

				picAdd.backgroundImage = '/img/picAdd.png';
				picAdd.addEventListener('click', addPiC);
				empty.image = "null";
			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

		Cloud.Users.update({
			photo: ""
		}, function(e) {
			if (e.success) {
				var user = e.users[0];
				//alert('Success:\n' + 'id: ' + user.id + '\n' + 'first name: ' + user.first_name + '\n' + 'last name: ' + user.last_name);
			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

	}

	function addPiC() {

		Titanium.Media.openPhotoGallery({

			success: function(event) {

				var image = event.media;
				//empty.backgroundImage = image;

				picAdd.removeEventListener('click', addPiC);
				picAdd.backgroundImage = '';
				empty.image = "/img/loading.png";

				if (prefs.theSize == 240) {
					pTags = "profile";
				} else {
					pTags = "";
				}

				Cloud.Photos.create({
					photo: image,
					tags: pTags,
					'photo_sync_sizes[]': "medium_640"
				}, function(e) {
					if (e.success) {

						if (prefs.theSize == 240) {

							Cloud.Users.update({
								photo_id: e.photos[0].id
							}, function(e) {
								if (e.success) {
									//var user = e.users[0];
									//alert('Success:\n' + 'id: ' + user.id + '\n' + 'first name: ' + user.first_name + '\n' + 'last name: ' + user.last_name);
								} else {
									alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
								}
							});

						}

						//if (prefs.theSize == 240) {

						Ti.App.fireEvent('updateProfilePhoto');
						//}

						var photo = e.photos[0];

						empty.cloudPhotoId = photo.id;
						empty.image = photo.urls.medium_640;
						picAdd.backgroundImage = '/img/picDel.png';
						picAdd.addEventListener('click', removePiC);

					} else {
						alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					}

				});

			},
			error: function(error) {
				if (error.code == Titanium.Media.NO_CAMERA) {
					alert('You have no camera');
				} else {
					alert('Camera error: ' + error.code);
				}
			}

		});

	}


	picAdd.addEventListener('click', addPiC);

	return frame;

}

module.exports = PhotoFrame;
