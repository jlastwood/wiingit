var Globals = require("/lib/Globals");

var LoginWithFB = require("/connections/LoginWithFB");

function MainController() {

	var Window;
	
	// first start

	if (!Ti.App.Properties.hasProperty("wasRun")) {

		Titanium.App.Properties.setBool("wasRun", true);

		Window = require("/ui/iphone/DemoBrowserScreen");
		
		new Window().open();

	// app has been started, but not logged in now

	} else if (!Ti.App.Properties.hasProperty("fbid")) {

		Window = require('/ui/iphone/LandingScreen');
		new Window().open();
		
	// is logged in, has wing	

	} else if (Ti.App.Properties.getBool('accepted')==true) {
		
		new LoginWithFB(function() {
			
			Window = require("/ui/iphone/ProfileBrowserScreen");
			new Window().open();
			
		});	
		
	// is logged in, no wing	
		
	} else {	

		new LoginWithFB(function() {

			Window = require('/ui/iphone/YourWingScreen');
			new Window().open();

		});

	}

}

module.exports = MainController;
