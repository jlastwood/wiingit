var Globals = require("/lib/Globals");

var Header = require("/ui/iphone/Header");

var IndividualScreen = require("/ui/iphone/IndividualScreen");

var GetPair = require("/connections/GetPair");
var GetProfilePhoto = require("/connections/GetProfilePhoto");

function ProfileBrowserScreen() {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white'

	});

	var peopleSeen = [];

	var upperHeader = new Header();

	self.add(upperHeader);

	var arena = Ti.UI.createView({
		top: 80,
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		zIndex: 40,
	});

	self.add(arena);

	var style = (Ti.Platform.name === 'iPhone OS') ? Ti.UI.iPhone.ActivityIndicatorStyle.DARK : Ti.UI.ActivityIndicatorStyle.BIG_DARK;

	var activityIndicator = Ti.UI.createActivityIndicator({
		color: '#ddd',
		font: {
			fontFamily: 'Helvetica Neue',
			fontSize: 26,
			fontWeight: 'bold'
		},
		message: 'Loading...',
		style: style,
		height: Ti.UI.FILL,
		width: Ti.UI.FILL
	});

	self.add(activityIndicator);

	var globUser = 0;

	var theLoc = {};

	// first we get location

	Globals.currentLoc(function(location) {

		Ti.API.info("START - got Loc");

		activityIndicator.show();
		theLoc = location;

		getNextUser(location.lon, location.lat, function(theUser) {

			peopleSeen.push(theUser.user.id);
			peopleSeen.push(theUser.wing);

			activityIndicator.hide();

			arena.add(viewMaker(theUser));

		});

		self.addEventListener('swipe', function(e) {

			//alert(globUser);

			if (e.direction == "left") {

				arena.children[0].animate({
					left: -640,
					duration: 200
				});

				// trash user

			} else if (e.direction == "right") {

				arena.children[0].animate({
					left: 320,
					duration: 200
				});

				//alert(arena.children[0].user.user);

				Cloud.ACLs.create({
					name: 'fav_rights',
					public_read: true,
					public_write: true
				}, function(e) {

					Cloud.Objects.create({
						classname: 'favorites',
						acl_name: 'fav_rights',
						fields: {
							liker: Ti.App.Properties.getString('currentUserId'),
							liked: arena.children[0].user.user.id
						}
					}, function(e) {
						alert("You have liked this profile and if they like you too, they will be added to your matches");
					});

				});

				// add to favorites

			}

			globUser++;

			activityIndicator.show();

			getNextUser(location.lon, location.lat, function(theUser) {

				Globals.removeChildren(arena);

				activityIndicator.hide();

				arena.add(viewMaker(theUser));

			});

		});

	});

	// helper functions

	function getNextUser(lon, lat, callback) {

		getUser(globUser, lon, lat, function(theUser) {

			if (theUser.user == Ti.App.Properties.getString('currentUserId') || theUser.wing == Ti.App.Properties.getString('currentUserId')) {

				Ti.API.info("OMITTED: Me or Wing");

				globUser = globUser + 1;

				getNextUser(lon, lat, callback);

			} else {

				callback(theUser);

			}

		});

	}

	function getUser(pageIndex, myLon, myLat, callback) {

		Ti.API.info("get User");

		new GetPair({
			counter: pageIndex,
			lat: myLat,
			lon: myLon,
			gender: Ti.App.Properties.getString("preferredGender") === "boys" ? "male" : "female",
			bornAfter: new Date().getFullYear() - Ti.App.Properties.getInt('maxAge'),
			bornBefore: new Date().getFullYear() - Ti.App.Properties.getInt('minAge'),
			maxDistance: Ti.App.Properties.getInt('maxDistance')
		}, function(e) {

			Ti.API.info(e);

			if (e.end) {

				activityIndicator.hide();

				var dialog = Ti.UI.createAlertDialog({
					message: 'The are no more users',
					buttonNames: ['Start over', 'OK'],
					title: 'End of queue'
				});

				dialog.addEventListener('click', function(e) {

					if (e.index == 0) {

						globUser = 0;
						activityIndicator.show();

						getNextUser(theLoc.lon, theLoc.lat, function(theUser) {

							activityIndicator.hide();
							Globals.removeChildren(arena);
							arena.add(viewMaker(theUser));

						});

					}

				});

				dialog.show();

			} else {

				var reply = {};

				reply.user = e.user1;

				reply.total = 100;

				reply.photo = "";

				reply.wing = e.user2;

				reply.wingphoto = "";

				callback(reply);

			}

		});

	}

	function viewMaker(theUser) {

		//alert(theUser.user.id);
		

		new GetProfilePhoto(theUser.user, function(photo) {

			img1.setImage(photo);
			activityIndicator1.hide();

		});

		new GetProfilePhoto(theUser.wing, function(photo) {

			img2.setImage(photo);
			activityIndicator2.hide();

		});

		var view = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: Ti.UI.FILL,
			layout: 'vertical',
			user: theUser
		});

		var cont1 = Ti.UI.createView({
			width: Titanium.Platform.displayCaps.platformHeight == 568 ? 600 / 2 : .7653 * 600 / 2,
			height: Titanium.Platform.displayCaps.platformHeight == 568 ? 392 / 2 : 300 / 2,
			top: 20,
			borderRadius: 1
		});

		var img1 = Ti.UI.createImageView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			defaultImage: "/images/loading.png"
			//defaultImage:"#eee"
		});

		cont1.add(img1);

		var cont2 = Ti.UI.createView({
			width: Titanium.Platform.displayCaps.platformHeight == 568 ? 600 / 2 : .7653 * 600 / 2,
			height: Titanium.Platform.displayCaps.platformHeight == 568 ? 392 / 2 : 300 / 2,
			top: 20,
			borderRadius: 1
		});

		var img2 = Ti.UI.createImageView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			//image: theUser.wingphoto
		});

		cont2.add(img2);


		var activityIndicator1 = Ti.UI.createActivityIndicator({
			color: '#ddd',
			font: {
				fontFamily: 'Helvetica Neue',
				fontSize: 26,
				fontWeight: 'bold'
			},
			message: '',
			style: style,
			height: Ti.UI.FILL,
			width: Ti.UI.FILL
		});

		img1.add(activityIndicator1);

		var activityIndicator2 = Ti.UI.createActivityIndicator({
			color: '#ddd',
			font: {
				fontFamily: 'Helvetica Neue',
				fontSize: 26,
				fontWeight: 'bold'
			},
			message: '',
			style: style,
			height: Ti.UI.FILL,
			width: Ti.UI.FILL
		});

		img2.add(activityIndicator2);

		activityIndicator1.show();
		activityIndicator2.show();


		view.add(cont1);
		view.add(cont2);

		cont1.addEventListener('click', function() {

			new IndividualScreen(theUser.user).open();

		});

		cont2.addEventListener('click', function() {

			new IndividualScreen(theUser.wing).open();

		});

		return view;

	}


	Ti.App.addEventListener('prefsChanged', function() {

		Globals.removeChildren(arena);

		globUser = 0;

		activityIndicator.show();

		getNextUser(theLoc.lon, theLoc.lat, function(theUser) {

			activityIndicator.hide();
			//self.remove(activityIndicator);

			arena.add(viewMaker(theUser));

		});

	});

	return self;
}

module.exports = ProfileBrowserScreen;
