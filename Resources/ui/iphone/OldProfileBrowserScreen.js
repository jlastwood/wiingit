var Globals = require("/lib/Globals");

var Header = require("/ui/iphone/Header");

var IndividualScreen = require("/ui/iphone/IndividualScreen");

function ProfileBrowserScreen() {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white'

	});

	var peopleSeen = [];

	var upperHeader = new Header();

	self.add(upperHeader);

	var arena = Ti.UI.createView({
		top: 80,
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		zIndex: 40,
	});

	self.add(arena);

	var style = (Ti.Platform.name === 'iPhone OS') ? Ti.UI.iPhone.ActivityIndicatorStyle.DARK : Ti.UI.ActivityIndicatorStyle.BIG_DARK;

	var activityIndicator = Ti.UI.createActivityIndicator({
		color: '#ddd',
		font: {
			fontFamily: 'Helvetica Neue',
			fontSize: 26,
			fontWeight: 'bold'
		},
		message: 'Loading...',
		style: style,
		height: Ti.UI.FILL,
		width: Ti.UI.FILL
	});

	self.add(activityIndicator);

	var globUser = 1;

	var theLoc = {};

	// first we get location

	Globals.currentLoc(function(location) {

		Ti.API.info("START - got Loc");

		activityIndicator.show();

		theLoc = location;

		getNextUser(location.lon, location.lat, function(theUser) {
			
			
			peopleSeen.push(theUser.user.id);
			peopleSeen.push(theUser.wing);
			

			activityIndicator.hide();
			//self.remove(activityIndicator);

			arena.add(viewMaker(theUser));

		});

		self.addEventListener('swipe', function(e) {

			//alert(globUser);

			if (e.direction == "left") {

				arena.children[0].animate({
					left: -640,
					duration: 200
				});

				// trash user

			} else if (e.direction == "right") {

				arena.children[0].animate({
					left: 320,
					duration: 200
				});

				//alert(arena.children[0].user.user);

				Cloud.ACLs.create({
					name: 'fav_rights',
					public_read: true,
					public_write: true
				}, function(e) {

					Cloud.Objects.create({
						classname: 'favorites',
						acl_name: 'fav_rights',
						fields: {
							liker: Ti.App.Properties.getString('currentUserId'),
							liked: arena.children[0].user.user.id
						}
					}, function(e) {
						alert("Added to your favorite matches!");
					});

				});

				// add to favorites

			}

			globUser++;
			//arena.add(activityIndicator);
			activityIndicator.show();

			//alert('You swiped to the ' + e.direction);

			/*arena.animate({
			 opacity: 0,
			 duration: 200
			 });*/

			getNextUser(location.lon, location.lat, function(theUser) {

				//alert(theUser);

				//if (theUser.isNextPossible == false) {

				//	globUser = 1;

				//finalUser = true;
				//}

				Globals.removeChildren(arena);

				activityIndicator.hide();

				arena.add(viewMaker(theUser));

			});

		});

	});

	// helper functions

	function getNextUser(lon, lat, callback) {

		getUser(globUser, lon, lat, function(theUser) {

			if (peopleSeen.indexOf(theUser.user.id)!==-1 || peopleSeen.indexOf(theUser.wing)!==-1 || theUser.wing == null || theUser.user.id == Ti.App.Properties.getString('currentUserId') || theUser.wing == Ti.App.Properties.getString('currentUserId')) {

				if (globUser < theUser.total) {

					globUser = globUser + 1;

					getNextUser(lon, lat, callback);

				} else {

					activityIndicator.hide();

					var dialog = Ti.UI.createAlertDialog({
						message: 'The are no more users',
						buttonNames: ['Start over', 'OK'],
						title: 'End of queue'
					});

					dialog.addEventListener('click', function(e) {

						if (e.index == 0) {

							globUser = 1;
							activityIndicator.show();
							peopleSeen=[];

							getNextUser(theLoc.lon, theLoc.lat, function(theUser) {

								activityIndicator.hide();
								Globals.removeChildren(arena);
								arena.add(viewMaker(theUser));

							});

						}

					});

					dialog.show();

				}

			} else {

				callback(theUser);

			}

		});

	}

	function getWing(userid, callback) {

		Ti.API.info("get Wing");

		Cloud.Friends.search({
			user_id: userid,
			per_page: 1,
			page: 1,
			response_json_depth: 2,
			sel: {
				"all": ["id", "photo"]
			},
		}, function(e) {
			if (e.success) {
				Ti.API.info("got Wing");
				Ti.API.info("-----------");
				//Ti.API.info(e.users[0].first_name);

				wing = null;

				if (e.users[0]) {
					wing = e.users[0];
				}

				//alert(wing);

				callback(wing);

			} else {
				//alert('Error:\\n' + ((e.error && e.message) || JSON.stringify(e)));
				activityIndicator.hide();
			}
		});

	}

	function getUser(pageIndex, myLon, myLat, callback) {

		Ti.API.info("get User");

		Cloud.Users.query({
			page: pageIndex,
			per_page: 1,
			response_json_depth: 2,
			//sel: {
			//	"all": ["id", "photo"]
			//},
			where: {

				//gend : {"$exists": true}

				gend: Ti.App.Properties.getString("preferredGender") === "boys" ? "male" : "female",
				"bornYear": {
					"$gte": new Date().getFullYear() - Ti.App.Properties.getInt('maxAge'),
					"$lte": new Date().getFullYear() - Ti.App.Properties.getInt('minAge'),
				},
				"coordinates": {
					'$nearSphere': [myLon, myLat],
					$maxDistance: Ti.App.Properties.getInt('maxDistance') / 3959
					//'$maxDistance': 0.00126*
				},
			}
		}, function(e) {


			if (e.success && e.users[0]) {

				var reply = {};

				reply.user = e.users[0];

				//aert("userid" + reply.user.id);

				reply.total = e.meta.total_results;

				if (e.users[0].photo) {
					reply.photo = e.users[0].photo.urls.medium_640;
				} else {
					reply.photo = "";
				}

				getWing(e.users[0].id, function(f) {

					//alert("wing" + f);

					if (f == null) {

						reply.wing = null;

						//alert("no wing");

						Ti.API.info("NOOOOOO WING\n___________________");

						callback(reply);

					} else {

						reply.wing = f.id;

						if (f.photo) {
							reply.wingphoto = f.photo.urls.medium_640;
						} else {
							reply.wingphoto = "";
						}

						//getProfilePhoto(reply.wing, function(h) {

						//alert("wingphoto" + h);

						//reply.wingphoto = h;

						callback(reply);

						//});

					}

				});

			} else {

				activityIndicator.hide();

				var dialog = Ti.UI.createAlertDialog({
					message: 'The are no more users',
					buttonNames: ['Start over', 'OK'],
					title: 'End of queue'
				});

				dialog.addEventListener('click', function(e) {

					if (e.index == 0) {

						globUser = 1;
						activityIndicator.show();

						getNextUser(theLoc.lon, theLoc.lat, function(theUser) {

							activityIndicator.hide();
							Globals.removeChildren(arena);
							arena.add(viewMaker(theUser));

						});

					}

				});

				dialog.show();

			}
		});

	}

	function viewMaker(theUser) {

		//alert(theUser.user.id);

		var view = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: Ti.UI.FILL,
			layout: 'vertical',
			user: theUser
		});

		var cont1 = Ti.UI.createView({
			width: Titanium.Platform.displayCaps.platformHeight == 568 ? 600 / 2 : .7653 * 600 / 2,
			height: Titanium.Platform.displayCaps.platformHeight == 568 ? 392 / 2 : 300 / 2,
			top: 20,
			borderRadius: 1
		});

		var img1 = Ti.UI.createImageView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			image: theUser.photo
		});

		cont1.add(img1);

		var cont2 = Ti.UI.createView({
			width: Titanium.Platform.displayCaps.platformHeight == 568 ? 600 / 2 : .7653 * 600 / 2,
			height: Titanium.Platform.displayCaps.platformHeight == 568 ? 392 / 2 : 300 / 2,
			top: 20,
			borderRadius: 1
		});

		var img2 = Ti.UI.createImageView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			image: theUser.wingphoto
		});

		cont2.add(img2);

		view.add(cont1);
		view.add(cont2);

		cont1.addEventListener('click', function() {

			new IndividualScreen(theUser.user.id).open();

		});

		cont2.addEventListener('click', function() {

			new IndividualScreen(theUser.wing).open();

		});

		return view;

	}


	Ti.App.addEventListener('prefsChanged', function() {

		Globals.removeChildren(arena);

		globUser = 1;

		activityIndicator.show();

		getNextUser(theLoc.lon, theLoc.lat, function(theUser) {

			activityIndicator.hide();
			//self.remove(activityIndicator);

			arena.add(viewMaker(theUser));

		});

	});

	return self;
}

module.exports = ProfileBrowserScreen;
