var Globals = require("/lib/Globals");

var Header = require("/ui/iphone/Header");

function IndividualScreen(userId) {

	var self = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: 'white',
	});

	var upperHeader = new Header("",1,self);

	self.add(upperHeader);
	
	theViews = [];


	var photosView = Ti.UI.createScrollableView({
		backgroundColor: 'white',
		showPagingControl: false,
		//views: [img1, img2, img3, img4],
		width: Ti.UI.FILL,
		height: Ti.UI.SIZE,
		top: 100
	});

	self.add(photosView);

	var nameText = Ti.UI.createLabel({
		text: "",
		color: "#4dc8f4",
		font: {
			fontSize: 28,
			fontWeight: 'bold'
		},
		top: 320,
		left: 20
	});

	self.add(nameText);

	var locText = Ti.UI.createLabel({
		text: "",
		color: "#999",
		font: {
			fontSize: 14
		},
		top: 330,
		right: 20
	});

	self.add(locText);

	var aboutText = Ti.UI.createLabel({
		text: "",
		color: "#999",
		font: {
			fontSize: 18,
			fontWeight: 'bold'
		},
		top: 370,
		left: 20
	});

	self.add(aboutText);

	var textArea = Ti.UI.createTextArea({
		editable: false,
		color: '#999',
		font: {
			fontSize: 14,
		},
		keyboardType: Ti.UI.KEYBOARD_NUMBER_PAD,
		returnKeyType: Ti.UI.RETURNKEY_GO,
		textAlign: 'left',
		value: '',
		top: 400,
		left: 20,
		right: 20,
		height: Titanium.Platform.displayCaps.platformHeight == 568 ? 120 : 50
	});

	self.add(textArea);

	var smallText = Ti.UI.createLabel({
		text: "", // "Last logged in 10 mins ago",
		color: "#999",
		font: {
			fontSize: 12,
			fontWeight: 'bold'
		},
		bottom: 10,
		right: 20
	});

	self.add(smallText);

	Cloud.Users.show({
		user_id: userId
	}, function(e) {
		if (e.success) {

			var user = e.users[0];

			var birthday = user.custom_fields.born;

			var birthYear = parseInt(birthday.split("/")[2]);

			var ageValue = birthYear > 0 ? new Date().getFullYear() - birthYear : "";

			nameText.text = (ageValue == "") ? Globals.capitalizeFirst(user.first_name) : Globals.capitalizeFirst(user.first_name) + " " + ageValue;

			aboutText.text = "About " + Globals.capitalizeFirst(user.first_name);

			textArea.value = user.custom_fields.description ? user.custom_fields.description : "";

		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});

	Cloud.Photos.query({
		page: 1,
		per_page: 20,
		where: {
			//tags_array: "profile",
			user_id: userId
		}
	}, function(e) {
		if (e.success) {

			//	alert(e.photos.length);

			var temp = [];

			for (var i = 0; i < e.photos.length; i++) {
				var photo = e.photos[i];

				var img = Ti.UI.createImageView({
					image: photo.urls.medium_640,
					height: 190,
					preventDefaultImage: true
				});

				photo.tags && photo.tags.indexOf("profile") !== -1 ? theViews.push(img) : temp.push(img);

			}

			theViews = theViews.concat(temp);

			photosView.views = theViews;

		} else {
			alert('UPL Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});

	Ti.App.addEventListener('logMeOut', function() {

		self.close();

	});

	return self;
}

module.exports = IndividualScreen;
