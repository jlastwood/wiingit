var Globals = require('/lib/Globals');
var Cloud = require('ti.cloud');
var Social = require('dk.napp.social');

var UpdateOurServer = require('/connections/UpdateOurServer');
var GetWing = require('/connections/GetWing');
var UpdateUser = require('/connections/UpdateUser');
var SubscribeToPushes = require('/connections/SubscribeToPushes');
var GetFriends = require('/connections/GetFriends');
var SetPreferences = require('/connections/SetPreferences');
var CheckInvited = require('/connections/CheckInvited');
var CheckAccepted = require('/connections/CheckAccepted');

function LoginWithFB(callback) {

	var clicked = 0;

	if (Social.isFacebookSupported() == false) {

		alert("You need to authorize your phone with Facebook");

	} else {

		Social.requestFacebook({
			requestType: "GET",
			url: "https://graph.facebook.com/v1.0/me",
			appIdKey: "442877975780077",
			callbackEvent: "facebookProfile",
			permissionsKey: "email,user_friends,friends_birthday,user_photos"
		}, {
			fields: 'id,name,gender,birthday'
		});

	}

	// STEP 1 - FB Login successful, saving profile data to persistent storage on
	// phone

	Social.addEventListener("facebookProfile", function(e) {

		Ti.API.info('step 1');

		new SetPreferences(e.response.id, e.response.name, e.response.birthday, e.response.gender);

		if (clicked == 0) {

			Social.requestFacebook({
				requestType: "GET",
				url: "https://graph.facebook.com/v1.0/me/friends",
				appIdKey: "442877975780077",
				callbackEvent: "facebookFriends",
				permissionsKey: "email,user_friends,friends_birthday"
			}, {
				fields: 'id,name,gender,birthday,username'
			});

			clicked++;

		}

	});

	// STEP 2 - getting Facebook friends and saving them in persistent storage

	Social.addEventListener("facebookFriends", function(e) {

		Ti.API.info("step 2");

		if (clicked == 1) {

			Ti.App.Properties.setObject("faceFriends", e.response.data);

			Cloud.SocialIntegrations.externalAccountLogin({
				type: 'facebook',
				token: e.accessToken
			}, function(f) {
				Ti.API.info(f);
				if (f.success) {

					var user = f.users[0];
					Ti.App.Properties.setString('currentUserId', user.id);

					new SubscribeToPushes(function() {

						Globals.getLoc(function(coords) {

							// update user data on cloud

							new UpdateUser(coords, function(coords) {

								new UpdateOurServer(coords, function() {

									callback(Ti.App.Properties.getObject("faceFriends"));

								});

							});

						});

					});

					clicked++;

				}

			});

		} else {

			//alert('SI Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});



}

module.exports = LoginWithFB;
