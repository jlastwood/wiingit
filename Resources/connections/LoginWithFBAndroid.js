var Globals = require('/lib/Globals');
var Cloud = require('ti.cloud');

var UpdateOurServer = require('/connections/UpdateOurServer');
var GetWing = require('/connections/GetWing');
var UpdateUser = require('/connections/UpdateUser');
var SubscribeToPushes = require('/connections/SubscribeToPushes');
var GetFriends = require('/connections/GetFriends');
var SetPreferences = require('/connections/SetPreferences');
var CheckInvited = require('/connections/CheckInvited');
var CheckAccepted = require('/connections/CheckAccepted');

var InviteScreen = require("/ui/android/InviteScreen");
var AcceptedScreen = require("/ui/android/AcceptedScreen");

function LoginWithFB(callback) {

	// STEP 1 - get my data

	var fb = require('facebook');
	fb.appid = "442877975780077";
	fb.permissions = ['email', 'user_friends', 'friends_birthday'];
	fb.forceDialogAuth = true;
	fb.addEventListener('login', function(e) {
		if (e.success) {

			fb.requestWithGraphPath('v1.0/me', {}, 'GET', function(e) {
				if (e.success) {

					new SetPreferences(e.result.id, e.result.name, e.result.birthday, e.result.gender);

					new GetFriends(fb, function(friendList) {

						Cloud.SocialIntegrations.externalAccountLogin({
							type: 'facebook',
							token: fb.accessToken
						}, function(f) {
							Ti.API.info(f);
							if (f.success) {

								var user = f.users[0];
								Ti.App.Properties.setString('currentUserId', user.id);

								// subscribe to pushes

								new SubscribeToPushes(function() {

									// check where we are geographically

									Globals.getLoc(function(coords) {

										// update user data on cloud

										new UpdateUser(coords, function(coords) {

											new UpdateOurServer(coords, function() {

												new CheckInvited(function(user) {

													if (user) {

														new InviteScreen(user.id, user.first_name, 0).open();

													} else {

														new CheckAccepted(function(user) {

															if (user) {

																new AcceptedScreen(user.id, user.first_name, 0).open();

															} else {

																callback(JSON.parse(friendList).data);

															}

														});

													}

												});

											});

										});

									});

								});

							}

						});

					});

				} else if (e.error) {
					alert(e.error);
				} else {
					alert('Unknown response');
				}
			});

			//callback();
		} else if (e.error) {
			alert(e.error);
		} else if (e.cancelled) {
			alert("Canceled");
		}
	});

	if (!fb.loggedIn) {
		fb.authorize();

	} else {

		fb.requestWithGraphPath('v1.0/me', {
			fields: 'id,name,gender,birthday'
		}, 'GET', function(e) {
			if (e.success) {

				var res = JSON.parse(e.result);

				new SetPreferences(res.id, res.name, res.birthday, res.gender);

				new GetFriends(fb, function(friendList) {

					Cloud.SocialIntegrations.externalAccountLogin({
						type: 'facebook',
						token: fb.accessToken
					}, function(f) {
						Ti.API.info(f);
						if (f.success) {

							var user = f.users[0];
							Ti.App.Properties.setString('currentUserId', user.id);

							// subscribe to pushes

							new SubscribeToPushes(function() {

								// check where we are geographically

								Globals.getLoc(function(coords) {

									// update user data on cloud

									new UpdateUser(coords, function(coords) {

										new UpdateOurServer(coords, function() {

											new CheckInvited(function(user) {

												if (user) {

													new InviteScreen(user.id, user.first_name, 0).open();

												} else {

													new CheckAccepted(function(user) {

														if (user) {

															new AcceptedScreen(user.id, user.first_name, 0).open();

														} else {

															callback(JSON.parse(friendList).data);

														}

													});

												}

											});

										});

									});

								});

							});

						}

					});

				});

			} else if (e.error) {
				alert(e.error);
			} else {
				alert('Unknown response');
			}
		});

	}

}

module.exports = LoginWithFB;
