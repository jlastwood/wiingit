var Globals = require("/lib/Globals");

function GetUserFromCloud(userid,callback) {

		Cloud.Users.show({
			user_id: userid
		}, function(e) {
			if (e.success) {

				var user = e.users[0];
				
				callback(user);


			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

}

module.exports = GetUserFromCloud;

