var Globals = require("/lib/Globals");

function GetProfilePhoto(userid, callback) {

	Cloud.Photos.query({
		page: 1,
		per_page: 1,
		where: {
			user_id: userid,
			tags_array: "profile"
		}

	}, function(e) {
		
		if (e.photos[0].urls.medium_640) {
			
			callback(e.photos[0].urls.medium_640);
		
		} else {
			
			callback("");
		}

	});

}

module.exports = GetProfilePhoto;

