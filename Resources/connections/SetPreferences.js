var Globals = require("/lib/Globals");

function SetPreferences(fbid, name, birthday, gender) {

	Ti.App.Properties.setString("fbid", fbid);
	Ti.App.Properties.setString("gender", gender);
	Ti.App.Properties.setString("birthday", birthday);
	Ti.App.Properties.setString("name", name);

	if (!Ti.App.Properties.hasProperty('preferredGender')) {

		if (gender == "male") {

			Ti.App.Properties.setString("preferredGender", "girls");

		} else {

			Ti.App.Properties.setString("preferredGender", "boys");

		}

	}

	if (!Ti.App.Properties.hasProperty('minAge'))
		Ti.App.Properties.setInt("minAge", 0);
	if (!Ti.App.Properties.hasProperty('maxAge'))
		Ti.App.Properties.setInt("maxAge", 100);
	if (!Ti.App.Properties.hasProperty('maxDistance'))
		Ti.App.Properties.setInt("maxDistance", 100);

	Ti.API.info("STATUS: Preferences set");

}

module.exports = SetPreferences;
