var Globals = require("/lib/Globals");

function CheckInvited(callback) {

		Cloud.Friends.requests(function(e) {

			//alert(e);

			if (e.success && e.meta.total_results > 0) {

				//alert(e);
				
				Ti.API.info("STATUS: Got invite");

				var user = e.friend_requests[0].user;

				callback(user);

			} else {
				
				Ti.API.info("STATUS: No invite");
				
				callback(false);
			}
		});

}

module.exports = CheckInvited;

