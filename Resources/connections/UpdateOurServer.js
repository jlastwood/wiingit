var Globals = require("/lib/Globals");

var LoginShadow = require('/connections/LoginShadow');

function UpdateOurServer(coords, callback) {

	var birthYear = parseInt(Ti.App.Properties.getString("birthday").split("/")[2]);

	var divDate = Ti.App.Properties.getString("birthday").split("/");

	var finDate = divDate[2] + "-" + divDate[1] + "-" + divDate[0];

	var names = Ti.App.Properties.getString("name").split(" ");

	var fN = names[0];
	var lN = names[names.length - 1];

	var bY = birthYear > 0 ? birthYear : 2014;

	new LoginShadow({
		fbid: Ti.App.Properties.getString("fbid"),
		cloudid: Ti.App.Properties.getString('currentUserId'),
		born: finDate,
		born_year: bY,
		gender: Ti.App.Properties.getString("gender"),
		pushtoken: Ti.App.Properties.getString("pushToken"),
		first_name: fN,
		last_name: lN,

		lat: coords.lat,
		lon: coords.lon
	}, function() {
		
		Ti.API.info("STATUS: Our server updated");

		callback();

	});

}

module.exports = UpdateOurServer;

