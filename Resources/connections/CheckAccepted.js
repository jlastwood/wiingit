var Globals = require("/lib/Globals");

function CheckAccepted(callback) {

	if (Ti.App.Properties.getBool('accepted') != true) {

		Cloud.Friends.search({
			user_id: Ti.App.Properties.getString('currentUserId')
		}, function(e) {

			//alert(e);

			if (e.success && e.meta.total_results > 0) {

				//alert(e);
				
				Ti.API.info("STATUS: Got accept");

				var user = e.users[0];

				Ti.App.Properties.setBool('accepted', true);
				
				callback(user);


			} else {
				
				Ti.API.info("STATUS: No accept");
				
				callback(false);
			}
			
			
			
		});

	} else {
		
		
		callback(false);
	}
}

module.exports = CheckAccepted;

