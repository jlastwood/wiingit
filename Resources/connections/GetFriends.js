var Globals = require("/lib/Globals");

function GetFriends(fb, callback) {

	fb.requestWithGraphPath('v1.0/me/friends', {
		fields: 'id,name,gender,birthday,username'
	}, 'GET', function(e) {
		if (e.success) {

			Ti.API.info("STATUS: Got friend list");

			// gather friends

			var friendList = e.result;

			Ti.App.Properties.setObject("faceFriends", JSON.parse(e.result).data);

			callback(friendList);


		} else if (e.error) {
			alert(e.error);
		} else {
			alert('Unknown response');
		}
	});

}

module.exports = GetFriends;

