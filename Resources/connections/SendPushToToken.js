var Globals = require("/lib/Globals");

function SendPushToToken(token,payload,callback) {

			Cloud.PushNotifications.notifyTokens({
				channel: 'wingit',
				to_tokens: token,
				payload: payload
			}, function(e) {
				if (e.success) {
					
					callback();
					
				} else {
					alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
				}
			});

}

module.exports = SendPushToToken;

