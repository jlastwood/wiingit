var Globals = require('/lib/Globals');
var Cloud = require('ti.cloud');
var Social = require('dk.napp.social');

var LoginShadow = require('/connections/LoginShadow');

function LoginWithFB(callback) {

	var clicked = 0;

	if (Social.isFacebookSupported() == false) {

		alert("You need to authorize your phone with Facebook");

	} else {

		Social.requestFacebook({
			requestType: "GET",
			url: "https://graph.facebook.com/v1.0/me",
			appIdKey: "442877975780077",
			callbackEvent: "facebookProfile",
			permissionsKey: "email,user_friends,friends_birthday"
		}, {
			fields: 'id,name,gender,birthday'
		});

	}

	// STEP 1 - FB Login successful, saving profile data to persistent storage on
	// phone

	Social.addEventListener("facebookProfile", function(e) {

		Ti.API.info('step 1');

		//alert(e.response.gender);
		Ti.App.Properties.setString("fbid", e.response.id);
		Ti.App.Properties.setString("gender", e.response.gender);
		Ti.App.Properties.setString("birthday", e.response.birthday);
		Ti.App.Properties.setString("name", e.response.name);

		if (!Ti.App.Properties.hasProperty('preferredGender')) {

			if (e.response.gender == "male") {

				Ti.App.Properties.setString("preferredGender", "girls");

			} else {

				Ti.App.Properties.setString("preferredGender", "boys");

			}

		}

		if (!Ti.App.Properties.hasProperty('minAge'))
			Ti.App.Properties.setInt("minAge", 0);
		if (!Ti.App.Properties.hasProperty('maxAge'))
			Ti.App.Properties.setInt("maxAge", 100);
		if (!Ti.App.Properties.hasProperty('maxDistance'))
			Ti.App.Properties.setInt("maxDistance", 100);

		if (clicked == 0) {

			Social.requestFacebook({
				requestType: "GET",
				url: "https://graph.facebook.com/v1.0/me/friends",
				appIdKey: "442877975780077",
				callbackEvent: "facebookFriends",
				permissionsKey: "email,user_friends,friends_birthday"
			}, {
				fields: 'id,name,gender,birthday,username'
			});

			clicked++;

		}

	});

	// STEP 2 - getting Facebook friends and saving them in persistent storage

	Social.addEventListener("facebookFriends", function(e) {

		Ti.API.info("step 2");

		if (clicked == 1) {

			Ti.App.Properties.setObject("faceFriends", e.response.data);

			Cloud.SocialIntegrations.externalAccountLogin({
				type: 'facebook',
				token: e.accessToken
			}, function(f) {
				Ti.API.info(f);
				if (f.success) {

					var user = f.users[0];
					Ti.App.Properties.setString('currentUserId', user.id);

					subscribeMe(function() {

						getLoc(function(coords) {

							updateMe(coords, function(coords) {

								var birthYear = parseInt(Ti.App.Properties.getString("birthday").split("/")[2]);
								
								var divDate = Ti.App.Properties.getString("birthday").split("/");
								
								var finDate = divDate[2]+"-"+divDate[1]+"-"+divDate[0];
								
								var names = Ti.App.Properties.getString("name").split(" ");
								
								var fN = names[0];
								var lN = names[names.length-1];

								
								

								var bY = birthYear > 0 ? birthYear : 2014;

								new LoginShadow({
									fbid: Ti.App.Properties.getString("fbid"),
									cloudid: Ti.App.Properties.getString('currentUserId'),
									born: finDate,
									born_year: bY,
									gender: Ti.App.Properties.getString("gender"),
									pushtoken: Ti.App.Properties.getString("pushToken"),
									first_name: fN,
									last_name: lN,
									
									lat: coords.lat,
									lon: coords.lon
								}, function() {

									callback();

								});

							});

						});

					});

					clicked++;

				}

			});

		} else {

			//alert('SI Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});

	// helper functions

	function subscribeMe(callmeback) {

		Cloud.PushNotifications.subscribe({
			device_token: Ti.App.Properties.getString("pushToken"),
			channel: 'wingit',
			type: Ti.Platform.name == 'android' ? 'gcm' : 'ios'
		}, function(h) {
			if (h.success) {

				callmeback();

			} else {

				alert('SU Error: ' + ((h.error && h.message) || JSON.stringify(h)));

				// TODELETE
				callmeback();
			}
		});

	}

	function updateMe(coords, callmeback) {

		var birthYear = parseInt(Ti.App.Properties.getString("birthday").split("/")[2]);

		var bY = birthYear > 0 ? birthYear : 2014;

		Cloud.Users.update({
			custom_fields: {
				born: Ti.App.Properties.getString("birthday"),
				bornYear: bY,
				gend: Ti.App.Properties.getString("gender"),
				facebookId: Ti.App.Properties.getString("fbid"),
				pushMe: Ti.App.Properties.getString("pushToken"),
				coordinates: [coords.lon, coords.lat]
			}

		}, function(g) {
			if (g.success) {

				//alert(g.users[0].id);

				getWing(g.users[0].id, function(rep) {

					//alert(rep);

					if (rep) {

						Ti.App.Properties.setString("currentWing", rep);
						Ti.App.Properties.setBool("accepted", true);

						//alert('yo');

					}

					callmeback(coords);

				});

			} else {
				alert('UPD Error:\n' + ((g.error && g.message) || JSON.stringify(g)));
			}
		});

		// delayed runtime getter

		setTimeout(function() {

			Cloud.Friends.requests(function(e) {

				//alert(e);

				if (e.success && e.meta.total_results > 0) {

					//alert(e);

					var user = e.friend_requests[0].user;

					new InviteScreen(user.id, user.first_name, 0).open();

				}
			});

			if (Ti.App.Properties.getBool('accepted') != true) {

				Cloud.Friends.search({
					user_id: Ti.App.Properties.getString('currentUserId')
				}, function(e) {

					//alert(e);

					if (e.success && e.meta.total_results > 0) {

						//alert(e);

						var user = e.users[0];

						Ti.App.Properties.setBool('accepted', true);

						new AcceptedScreen(user.id, user.first_name, 0).open();

					}
				});

			}

		}, 5000);

		// check if profile photo exists

		Cloud.Photos.query({
			page: 1,
			per_page: 20,
			where: {
				tags_array: "profile",
				user_id: Ti.App.Properties.getString('currentUserId')
			}
		}, function(e) {
			if (e.success) {

				//	alert(e.photos.length);

				if (e.photos.length == 0) {

					// if no profile photo, upload it from facebook

					var xhr = Titanium.Network.createHTTPClient({
						onload: function() {

							Cloud.Photos.create({
								photo: this.responseData,
								tags: 'profile'
							}, function(f) {

								//alert(e);

								if (f.success) {

								} else {
									alert('Error:\n' + ((f.error && f.message) || JSON.stringify(f)));
								}
							});

						},
						timeout: 10000
					});
					xhr.open('GET', 'https://graph.facebook.com/' + Ti.App.Properties.getString("fbid") + '/picture?width=640&height=640');
					xhr.send();

				}

			} else {
				alert('UPL Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});

	}

	function getWing(userid, callback) {

		Cloud.Friends.search({
			user_id: userid
		}, function(e) {

			//alert(e);

			if (e.success) {

				wing = null;

				if (e.users[0]) {
					wing = e.users[0];
				} else {

					wing = {
						id: null
					};
				}

				//alert(wing);

				callback(wing.id);

			} else {
				alert('Error:\\n' + ((e.error && e.message) || JSON.stringify(e)));
				activityIndicator.hide();
			}
		});

	}

	function getLoc(callback) {

		Ti.Geolocation.getCurrentPosition(function(e) {

			if (e.error) {
				Ti.API.error("Geolocation error:" + e.error);
				return;
			} else {

				if (e.coords.latitude !== undefined && e.coords.longitude !== undefined) {

					callback({
						lat: e.coords.latitude,
						lon: e.coords.longitude
					});

				}

			}

		});

	}

}

module.exports = LoginWithFB;
