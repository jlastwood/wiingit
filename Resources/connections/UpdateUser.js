var Globals = require("/lib/Globals");

var GetWing = require('/connections/GetWing');
var CheckInvited = require('/connections/CheckInvited');

function UpdateUser(coords, callmeback) {

	var birthYear = parseInt(Ti.App.Properties.getString("birthday").split("/")[2]);

	var bY = birthYear > 0 ? birthYear : 2014;

	Cloud.Users.update({
		custom_fields: {
			born: Ti.App.Properties.getString("birthday"),
			bornYear: bY,
			gend: Ti.App.Properties.getString("gender"),
			facebookId: Ti.App.Properties.getString("fbid"),
			pushMe: Ti.App.Properties.getString("pushToken"),
			coordinates: [coords.lon, coords.lat]
		}

	}, function(g) {
		if (g.success) {
			
			Ti.API.info("STATUS: User data updated");

			//alert(g.users[0].id);

			new GetWing(g.users[0].id, function(rep) {

				//alert(rep);

				if (rep) {

					Ti.App.Properties.setString("currentWing", rep);
					Ti.App.Properties.setBool("accepted", true);

					//alert('yo');

				}

				callmeback(coords);

			});

		} else {
			alert('UPD Error:\n' + ((g.error && g.message) || JSON.stringify(g)));
		}
	});



	// check if profile photo exists

	Cloud.Photos.query({
		page: 1,
		per_page: 20,
		where: {
			tags_array: "profile",
			user_id: Ti.App.Properties.getString('currentUserId')
		}
	}, function(e) {
		if (e.success) {

			//	alert(e.photos.length);

			if (e.photos.length == 0) {

				// if no profile photo, upload it from facebook

				var xhr = Titanium.Network.createHTTPClient({
					onload: function() {

						Cloud.Photos.create({
							photo: this.responseData,
							tags: 'profile'
						}, function(f) {

							//alert(e);

							if (f.success) {

							} else {
								alert('Error:\n' + ((f.error && f.message) || JSON.stringify(f)));
							}
						});

					},
					timeout: 10000
				});
				xhr.open('GET', 'https://graph.facebook.com/' + Ti.App.Properties.getString("fbid") + '/picture?width=640&height=640');
				xhr.send();

			}

		} else {
			alert('UPL Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});

}

module.exports = UpdateUser;
