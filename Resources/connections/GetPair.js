var Globals = require('lib/Globals');


function GetPair(props,callback) {	

    var url = Globals.apiRoot;
    url+="pair/"+props.counter;
    url+="/"+props.lat;
    url+="/"+props.lon;
    url+="/"+props.gender;
    url+="/"+props.bornAfter;    
    url+="/"+props.bornBefore;  
    url+="/"+props.maxDistance;
        
	Ti.API.info("GET PAIR: "+url);
	 
	  
    var client = Ti.Network.createHTTPClient({
	 	
        onsendstream: function(e) {

        },
		
        onload : function(e) {
        	
        	//callback();
        	
        	//alert(e.responseText);
        	var reply = JSON.parse(this.responseText);

            	callback(reply);        		
	     	
            
            //callback(eval('('+this.responseText+')'));

        },
	     
        onerror : function(e) {
	     	
            alert(L('connection_error'));

        },

        timeout : 5000  
    });
	 
    client.cache=false;

    client.open("GET", url);

    client.send(); 
		
	
}



module.exports = GetPair;
