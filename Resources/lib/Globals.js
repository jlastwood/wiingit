exports.apiRoot = "...";

exports.getLoc = function(callback) {

	Ti.Geolocation.getCurrentPosition(function(e) {

		if (e.error) {
			Ti.API.error("Geolocation error:" + e.error);
			return;
		} else {

			if (e.coords.latitude !== undefined && e.coords.longitude !== undefined) {

				callback({
					lat: e.coords.latitude,
					lon: e.coords.longitude
				});

			}

		}

	});

};


exports.cap = function(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
};

exports.is7 = function() {
	// iOS-specific test
	if (Titanium.Platform.name == 'iPhone OS') {
		var version = Titanium.Platform.version.split(".");
		var major = parseInt(version[0], 10);

		// Can only test this support on a 3.2+ device
		if (major >= 7) {
			return true;
		}
	}
	return false;
};

var lat = null;
var lon = null;

exports.setLocation = function(la, lo) {

	lat = la;
	lon = lo;
};

exports.getLocation = function() {

	var loc = {
		lat: lat,
		lon: lon
	};

	return loc;
};

exports.currentLoc = function(callback) {

	Ti.Geolocation.getCurrentPosition(function(e) {

		if (e.error) {
			Ti.API.error("Geolocation error:" + e.error);
			return;
		} else {

			if (e.coords.latitude !== undefined && e.coords.longitude !== undefined) {

				callback({
					lat: e.coords.latitude,
					lon: e.coords.longitude
				});

			}

		}

	});

};

exports.removeChildren = function(viewObject) {

	var children = viewObject.children.slice(0);

	for (var i = 0; i < children.length; ++i) {
		viewObject.remove(children[i]);
	}
};

exports.capitalizeFirst = function(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
};

exports.lorem = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

