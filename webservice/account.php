<?php


require_once 'helper_functions.php';

class account {



	function beforeRoute() {

		header('Content-Type: application/json');

	}



	function login($f3) {

		$db=Helper::dbConnect();

		$user=new DB\SQL\Mapper($db, 'users');

		// step1 - check if token existd


		if ($f3->get('GET.fbid')>0) {


			$user->load(array('fbid=?', $f3->get('GET.fbid') ) );

			$user->cloudid = $f3->get('GET.cloudid');
			$user->pushtoken = $f3->get('GET.pushtoken');
			$user->first_name = $f3->get('GET.first_name');
			$user->last_name = $f3->get('GET.last_name');
			$user->lat = $f3->get('GET.lat');
			$user->lon = $f3->get('GET.lon');
			$user->born = $f3->get('GET.born');
			$user->born_year = $f3->get('GET.born_year');
			$user->gender = $f3->get('GET.gender');
			$user->fbid = $f3->get('GET.fbid');

			$user->save();

			print($user->id);


		} else {

			print("fail");
		}


	}





	function wing($f3) {

		$db=Helper::dbConnect();


		$db->exec(
			'DELETE FROM friendships WHERE user1=? OR user2=?',
			array(1=>$f3->get('GET.user1'),$f3->get('GET.user1'))
			);


		$db->exec(
			'DELETE FROM friendships WHERE user1=? OR user2=?',
			array(1=>$f3->get('GET.user1'),$f3->get('GET.user1'))
			);



		$db->exec(
			'INSERT INTO friendships (user1,user2) VALUES(?,?)',
			array(1=>$f3->get('GET.user1'),$f3->get('GET.user2'))
			);


	}







	function unwing($f3) {

		$db=Helper::dbConnect();

		// step1 - check if token existd


		$w=$db->exec(
			'DELETE FROM friendships WHERE user1=? OR user2=?',
			array(1=>$f3->get('GET.user'),2=>$f3->get('GET.user'))
			);

		print($w);



	}



	function pair($f3) {

		$db=Helper::dbConnect();

		if ($f3->get('PARAMS.maxDistance')==0) {

			$dist = 10000;
		
		} else {

			$dist = $f3->get('PARAMS.maxDistance');
		}





		$result = $db->exec(
					'SELECT f.*, u.lat, u.lon, '.
					'u.fbid AS u1_fbid, '.
					'(SELECT w.fbid FROM users w WHERE w.cloudid = f.user2) AS u2_fbid, '.  
					'( 3959 * acos( cos( radians(?) ) * cos( radians( u.lat ) ) * cos( radians( u.lon ) - radians(?) ) + sin( radians(?) ) * sin( radians( u.lat ) ) ) ) AS distance '.
					'FROM friendships f '.
					'JOIN users u on u.cloudid = f.user1 '.
					'WHERE (u.gender=? AND '.
					'u.born_year>? AND '.
					'u.born_year<?) '.
					'HAVING distance < ? '.
					'ORDER BY distance LIMIT ?,1',
					array(
						1=>$f3->get('PARAMS.lat'),
						2=>$f3->get('PARAMS.lon'),
						3=>$f3->get('PARAMS.lat'), 
						4=>$f3->get('PARAMS.gender'), 
						5=>$f3->get('PARAMS.bornAfter'), 
						6=>$f3->get('PARAMS.bornBefore'), 																		
						7=>$dist, 
						8=>$f3->get('PARAMS.counter')
						)
					);
		

		if (array_key_exists(0, $result)) {$reply=$result[0];} else {$reply['end']=true;}

		$reply['counter']    = $f3->get('PARAMS.counter');
		$reply['lat']        = $f3->get('PARAMS.lat');
		$reply['lon']        = $f3->get('PARAMS.lon');
		$reply['gender']     = $f3->get('PARAMS.gender');
		$reply['bornAfter']  = $f3->get('PARAMS.bornAfter');
		$reply['bornBefore'] = $f3->get('PARAMS.bornBefore');						
		$reply['maxDistance'] = $f3->get('PARAMS.maxDistance');	

		print(json_encode($reply));





	}






}




?>
