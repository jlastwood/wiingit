<?php


class Helper {

	public static function dbConnect() {

		return new DB\SQL(

			'mysql:host:localhost;port=3306;dbname=wingit',
			'',
			''
		);

	}



	public static function checkToken($f3) {

		$testedToken = $f3->get('GET.token');

		$db = Helper::dbConnect();

		$tokens = new DB\SQL\Mapper($db,'tokens');

		$tokens->load(array('token=?',$testedToken));


		if ($tokens->dry()) {

			return false;

		} else {

			return $tokens->userid;
		}

	}


	public static function showError($message) {

		$reply = ["status"=>"error","description"=>$message];

		print(json_encode($reply));

	}


}

?>